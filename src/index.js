import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import './styles/_bootstrap-override.scss'
import './styles/_variables.scss'
import App from './pages/_app/App';
import reportWebVitals from './reportWebVitals';
import 'bootstrap/dist/css/bootstrap.min.css';
import mapboxgl from 'mapbox-gl';

mapboxgl.accessToken = 'pk.eyJ1IjoiYWxiYW5wYXBhc3NpYW4iLCJhIjoiY2tmaTk0ZHd2MHN5bDJybnZqZHR2ZjdlbCJ9.willpc29lmtz1LxaTyES-A';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your token, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
