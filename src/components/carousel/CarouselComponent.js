/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable no-useless-constructor */
import { BaseComponent } from '../_base-component';
import './CarouselComponent.scss';
import Carousel from 'react-bootstrap/Carousel'


export class CarouselComponent extends BaseComponent {
    constructor() {
        super();
    }

    render() {

        return (
            <Carousel className="carousel-component">
                <Carousel.Item>
                    <div className="carousel-image d-block w-100" style={{ backgroundImage: 'url(https://images.wallpaperscraft.com/image/concert_performance_light_organ_137789_1920x1080.jpg)' }}>
                    </div>
                    <Carousel.Caption>
                        <h3>First slide label</h3>
                        <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <div className="carousel-image d-block w-100" style={{ backgroundImage: 'url(https://images.wallpaperscraft.com/image/concert_party_performance_138075_1920x1080.jpg)' }}>
                    </div>
                    <Carousel.Caption>
                        <h3>First slide label</h3>
                        <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <div className="carousel-image d-block w-100" style={{ backgroundImage: 'url(https://images.wallpaperscraft.com/image/panda_guitarist_art_128406_1920x1080.jpg)' }}>
                    </div>
                    <Carousel.Caption>
                        <h3>First slide label</h3>
                        <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                    </Carousel.Caption>
                </Carousel.Item>
            </Carousel>

        );
    }
}