import React from 'react';
import './TokenComponent.scss';
import { Routes } from '../../constantes/routes';
import {Link} from "react-router-dom";

function setToken(userToken) {
    sessionStorage.setItem('token', JSON.stringify(userToken));
}

function getToken() {
    const tokenString = sessionStorage.getItem('token');
    const userToken = JSON.parse(tokenString);
    return userToken?.token
}

function App() {
    const token = getToken();

    if(!token) {
        return <Login setToken={setToken} />
    }

    return (
        <div className="wrapper">
            ...
        </div>
    );
}

export default App;