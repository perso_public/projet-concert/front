import './CartContentComponent.scss';
import { BaseComponent } from '../_base-component';
import Moment from 'react-moment';
import 'moment/locale/fr';


export class CartContentComponent extends BaseComponent {
    obtainingPrice;
    constructor() {
        super();

        this.state = {
            ticketsHtml: [],
            obtainingMethodsHtml: [],
            tickets: [],
            concert: [],
            total: 0,
            bin: true,
        }

        this.obtainingPrice = {
            'E-Ticket': 0,
            'Retrait au guichet': 1.8,
            'Envoi postal': 8
        }
    }

    printTickets() {
        let ticketsHtml = [];
        let obtainingMethodsHtml = [];
        let i = 1;
        let total = 0;
        if(this.state.tickets) {
            for(const ticket of this.state.tickets) {
                ticketsHtml.push( <tr>
                    <td>{i}.</td>
                    <td>1 Place</td>
                    <td>{this.state.concert?.musicGroup?.name}</td>
                    <td>{this.state.concert?.concertHall?.location?.name}</td>
                    <td><Moment format="dddd D MMMM YYYY" locale="fr">{this.state.concert.date}</Moment> <br/>
                        <Moment format="HH:mm" locale="fr">{this.state.concert.date}</Moment>
                    </td>
                    <td>{ticket.concertTicket?.category?.value}</td>
                    <td>{ticket.price} €</td>
                    <td>
                        {this.state.bin ? <button className="btn" onClick={() => this.deleteTicket(this.state.tickets.indexOf(ticket))}><i className="bi bi-trash"></i></button> : ''}
                    </td>
                </tr>)

                obtainingMethodsHtml.push(<tr>
                    <td></td>
                    <td>{this.state.concert?.musicGroup?.name}</td>
                    <td>{ticket.obtainingMethod?.value}</td>
                    <td className="text-end">{this.obtainingPrice[ticket.obtainingMethod.value] == 0 ? 'Gratuit' : `${this.obtainingPrice[ticket.obtainingMethod.value]} €`}</td>
                </tr>)
                i += 1;
                total += ticket.price;
                total += this.obtainingPrice[ticket.obtainingMethod.value];
            }
        }
        console.log(this.state.tickets);

        this.setState({ ticketsHtml: ticketsHtml, obtainingMethodsHtml: obtainingMethodsHtml, total: total });
    }

    deleteTicket(index) {
        this.state.tickets.splice(index,1);
        localStorage.setItem('ticketsReservation', JSON.stringify(this.state.tickets));
        this.componentDidMount();
    }

    componentDidMount() {
        this.setState({ concert: this.props.concert, tickets: this.props.tickets, bin: this.props.bin}, this.printTickets);
    }

    render() {
        return (
            <div>
                <div className="cart-component">
                    <div className="row my-3 py-3">
                        <p>
                            <u> Récapitulatif de votre panier</u>
                        </p>

                        <table className="table cart-table">
                            <thead>
                                <tr>
                                    <th scope="col">N°</th>
                                    <th scope="col">Nombre de place(s)</th>
                                    <th scope="col">Artiste / Groupe</th>
                                    <th scope="col">Lieu</th>
                                    <th scope="col">Date et heure</th>
                                    <th scope="col">Catégorie des tarifs</th>
                                    <th scope="col">Tarifs</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.ticketsHtml}
                            </tbody>
                        </table>
                    </div>



                    <div>
                        <table className="table">
                            <tbody>
                                <tr>
                                    <td>Obtention des billets</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                {this.state.obtainingMethodsHtml}
                            </tbody>
                        </table>
                    </div>

                    <div className="cart__total">
                        <div>
                            MONTANT TOTAL DU PANIER
                    </div>
                        <div>
                            {this.state.total} €
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}