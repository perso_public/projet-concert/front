/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable no-useless-constructor */
import { Routes } from '../../constantes/routes';
import { BaseComponent } from '../_base-component';
import './NavbarComponent.scss';
import { Link } from 'react-router-dom';
import { Redirect } from "react-router-dom";

export class NavbarComponent extends BaseComponent {

    constructor(props) {
        super(props);
        this.disconnection = this.disconnection.bind(this);

        const ticketsReservation = JSON.parse(localStorage.getItem('ticketsReservation'));
        if (ticketsReservation)
            this.number = ticketsReservation.length;
    }

    state = { redirect: null };

    disconnection() {
        const route = "/"
        localStorage.removeItem('user');
        console.log('c\'est bon');
        this.setState({ redirect: route });
    }

    render() {
        this.concertId = localStorage.getItem('concertId');
        if (this.state.redirect) {
            return <Redirect to={this.state.redirect} />
        }
        let button;
        let admin;
        const user = JSON.parse(localStorage.getItem('user'));
        if (user) {
            const roles = user.roles.includes('ROLE_ADMIN');

            if (roles === true) {
                admin = <Link className="nav-link active" to={Routes.AdminListConcerts}> Administration</Link>
            }
        }

        if (user) {
            button = <Link className="nav-link active" onClick={this.disconnection}> Déconnexion</Link>
        } else {
            button = <Link className="nav-link active" to={Routes.RegisterPage}> Inscription</Link>;
        }
        return (
            <div>
                <nav className="navbar navbar-expand-lg navbar-light bg-light pt-4 pb-4">
                    <div className="container-fluid">
                        <Link className="navbar-brand" to={{ pathname: Routes.Home }}>
                            {/* <img src="https://i.dlpng.com/static/png/6408414_preview.png" alt="" className="" /> */}
                        </Link>

                        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="collapse navbar-collapse justify-content-between flex-column" id="navbarNav">
                            <div className="navBarContainer-links">
                                <ul className="navbar-nav">
                                    <li className="nav-item">
                                        <a className="nav-link" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                            Programmation
                                    </a>
                                        <ul className="dropdown-menu">
                                            <li><a className="dropdown-item" href={Routes.Programmation}> Tous les évènements</a></li>
                                            <li><Link className="dropdown-item" to={{ pathname: Routes.Programmation, data: { location: 'Aix-en-Provence' } }}>Aix-en-Provence</Link></li>
                                            <li><Link className="dropdown-item" to={{ pathname: Routes.Programmation, data: { location: 'Bourges' } }}>Bourges</Link></li>
                                            <li><Link className="dropdown-item" to={{ pathname: Routes.Programmation, data: { location: 'Cannes' } }}>Cannes</Link></li>
                                            <li><Link className="dropdown-item" to={{ pathname: Routes.Programmation, data: { location: 'Dunkerque' } }}>Dunkerque</Link></li>
                                            <li><Link className="dropdown-item" to={{ pathname: Routes.Programmation, data: { location: 'Echirolles' } }}>Echirolles</Link></li>

                                            {/* <li><a className="dropdown-item" href="#"> Bourges </a></li>
                                            <li><a className="dropdown-item" href="#"> Cannes </a></li>
                                            <li><a className="dropdown-item" href="#"> Dunkerque </a></li>
                                            <li><a className="dropdown-item" href="#"> Echirolles </a></li> */}
                                            {/* <li><a className="dropdown-item" href="#"> Comment réserver ? </a></li> */}
                                        </ul>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">restauration</a>
                                        <ul className="dropdown-menu">
                                            <li><a className="dropdown-item"> Présentation </a></li>
                                            <li><a className="dropdown-item"> Réserver </a></li>
                                        </ul>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">parking</a>
                                        <ul className="dropdown-menu">
                                            <li><a className="dropdown-item"> Présentation </a></li>
                                            <li><a className="dropdown-item"> Réserver </a></li>
                                        </ul>
                                    </li>
                                    <li className="nav-item">
                                        <Link className="nav-link" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">privatisation</Link>
                                        <ul className="dropdown-menu">
                                            <li><Link className="dropdown-item" to={{ pathname: Routes.PrivatisationPage }}> Présentation </Link></li>
                                            <li><Link className="dropdown-item" to={{ pathname: Routes.PrivatisationReservationPage }}> Réserver </Link></li>
                                        </ul>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">actualités</a>
                                        <ul className="dropdown-menu">
                                            <li><a className="dropdown-item"> Présentation </a></li>
                                            <li><a className="dropdown-item"> Réserver </a></li>
                                        </ul>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">infos pratiques</a>
                                        <ul className="dropdown-menu">
                                            <li><a className="dropdown-item"> Présentation </a></li>
                                            <li><a className="dropdown-item"> Réserver </a></li>
                                        </ul>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link" role="button" data-bs-toggle="dropdown" aria-expanded="false">contact</a>
                                    </li>
                                </ul>

                                <div>
                                    <ul className="navbar-nav navbar-account">
                                        <li className="nav-item">
                                            <Link className="nav-link active" to={Routes.LoginPage}> Mon compte</Link>
                                        </li>
                                        <li className="nav-item no-style">
                                            <span className="nav-link">/</span>
                                        </li>
                                        <li className="nav-item">
                                            {button}
                                        </li>
                                        <li className="nav-item">
                                            {admin}
                                        </li>
                                        <li className="nav-item no-style ms-3">
                                            {/* <a href="" className="navbar__basket"> */}
                                            <Link className="navbar__basket" to={Routes.Cart + '/' + this.concertId}>
                                                <i className="bi bi-basket"></i>
                                                {this.number ? <div>{this.number}</div> : ''}
                                            </Link>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            {/* <form className="d-flex">
                                <input className="form-control me-2 searchBar" type="search" placeholder="Search" aria-label="Search" />
                                <button className="btn btn-primary" type="submit">Search</button>
                            </form> */}
                        </div>
                    </div>
                </nav>
            </div>
        );
    }
}
