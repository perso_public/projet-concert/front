import './TicketsPlanComponent.scss';
import { BaseComponent } from '../_base-component';

export class TicketsPlanComponent extends BaseComponent {

    planTickets = [];

    constructor() {
        super();
    }

    render() {
        this.planTickets = this.props.planTickets;

        return (
            <div className="TicketsPlan">
                <div className="plan__seatPicker-container col-md-10 col-sm-12">
                    <div className="plan__seatPicker-header">scène</div>

                    <div className="plan__seatPicker">
                        <div className="left-col">
                            <span>A</span>
                            <span>B</span>
                            <span>C</span>
                            <span>D</span>
                            <span>E</span>
                            <span>F</span>
                            <span>G</span>
                            <span>H</span>
                            <span>I</span>
                        </div>

                        <div className="top-col">
                            <span>1</span>
                            <span>2</span>
                            <span>3</span>
                            <span>4</span>
                            <span>5</span>
                            <span>6</span>
                            <span>7</span>
                            <span>8</span>
                            <span>9</span>
                            <span>10</span>
                            <span>11</span>
                            <span>12</span>
                        </div>

                        <div className="plan__seatPicker-places">
                            {this.planTickets ? this.planTickets.map(x => (
                                <div className="flex">
                                    {x.map(y => (
                                        <ul>
                                            <li key={y.key}>
                                                <input aria-label={(y.booked ? 'disabled' : '')} type="checkbox" key={'input_' + y.key} checked={y.checked} onChange={() => this.handleChangeChk(y.key)} disabled={y.booked} />
                                                <label aria-label={(y.booked ? 'disabled' : '')} htmlFor="checkbox-p" key={'label_' + y.key}></label>
                                            </li>
                                        </ul>
                                    ))}
                                </div>
                            )) : ''}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}