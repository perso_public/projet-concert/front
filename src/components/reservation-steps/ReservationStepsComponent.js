import './ReservationStepsComponent.scss';
import { BaseComponent } from '../_base-component';

export class ReservationStepsComponent extends BaseComponent {
    constructor() {
        super();
    }

    render() {
        return (
            <div className="header">
                <p className="active">1. Réservation</p>
                <p>2. Panier d'achat</p>
                <p>3. Coordonnées</p>
                <p>4. Paiement</p>
                <p>5. Confirmation</p>
            </div>
        );
    }
}