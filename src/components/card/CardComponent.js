import './CardComponent.scss';
import { BaseComponent } from '../_base-component';

export class CardComponent extends BaseComponent {
    constructor() {
        super();
    }

    render() {
        return (
            <div className="CardComponent">
                {this.props.text}
            </div>
        );
    }
}