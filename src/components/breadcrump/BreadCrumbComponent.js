import './BreadCrumbComponent.scss';
import { BaseComponent } from '../_base-component';

export class BreadCrumbComponent extends BaseComponent {
    constructor() {
        super();
    }

    render() {
        return (
            <div className="header">
                <p className={this.props.selectedStep == 1 ? 'active' : ''}>1. Réservation</p>
                <i className="bi bi-chevron-right"></i>
                <p className={this.props.selectedStep == 2 ? 'active' : ''}>2. Panier d'achat</p>
                <i className="bi bi-chevron-right"></i>
                <p className={this.props.selectedStep == 3 ? 'active' : ''}>3. Coordonnées</p>
                <i className="bi bi-chevron-right"></i>
                <p className={this.props.selectedStep == 4 ? 'active' : ''}>4. Paiement</p>
                <i className="bi bi-chevron-right"></i>
                <p className={this.props.selectedStep == 5 ? 'active' : ''}>5. Confirmation</p>
            </div>
        );
    }
}