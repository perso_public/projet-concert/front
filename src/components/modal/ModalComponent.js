/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable no-useless-constructor */
import { BaseComponent } from '../_base-component';
import './ModalComponent.scss';
import { Modal, Button } from 'react-bootstrap/'
import { useState } from 'react';
import { Redirect, useHistory } from 'react-router-dom';
import { Routes } from '../../constantes/routes';


function MyVerticallyCenteredModal(props) {
    return (
        <Modal
            {...props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header>
                <Modal.Title id="contained-modal-title-vcenter">
                    Attention
          </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <p>
                    Etes-vous sûr(e) de vouloir annuler ?
          </p>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="primary" onClick={props.onHide}>Non</Button>
                <Button variant="primary" onClick={props.onRedirect}>oui</Button>
            </Modal.Footer>
        </Modal>
    );
}

function App(props) {
    let history = useHistory();
    const route = props.route;

    const redirect = () => {
        history.push(route);
    }
    const [modalShow, setModalShow] = useState(false);
    return (
        <>
            <Button variant="danger" onClick={() => setModalShow(true)}>
                Annuler
            </Button>

            <MyVerticallyCenteredModal
                show={modalShow}
                onHide={() => setModalShow(false)}
                onRedirect={redirect}
            />
        </>
    );
}

export default App;