/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable no-useless-constructor */
import { BaseComponent } from '../_base-component';
import './LoaderComponent.scss';
import { Spinner } from 'react-bootstrap';

export class LoaderComponent extends BaseComponent {
    constructor() {
        super();
    }

    render() {
        return (
            <div className="spinner-component">
                <Spinner animation="border" role="status">
                    <span className="sr-only"></span>
                </Spinner>
            </div>
        );
    }
}