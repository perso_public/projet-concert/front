/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable no-useless-constructor */
import { BaseComponent } from '../_base-component';
import './VignettesComponent.scss';

export class VignetteNewsComponent extends BaseComponent {
    constructor() {
        super();
    }

    render() {
        return (
            <div className="card-container col-12 col-md-6 col-lg-3">
                <div className="card card--actualites shadow">
                    <div className="row g-0">
                        <div className="col-md-12 card-header-img card-header-img--actualites">
                            <img src="https://img.huffingtonpost.com/asset/5f1a00b8270000b10fe674d7.jpeg?cache=vM0RbxN9YX&ops=scalefit_630_noupscale" alt="" />
                        </div>

                        <div className="col-md-12 d-flex justify-content-center">
                            <div className="card-body overflow">
                                <h5 className="card-title">Titre actualité</h5>
                                <p className="card-text line-clamp">Lorem ipsum dolor sit amet consectetur adipisicing elit. Natus corporis, in officiis distinctio eaque quis unde quam modi maiores quos repellendus, suscipit assumenda cupiditate hic maxime. Odio alias in cum.</p>
                            </div>

                            <a href="1">
                                <span className="fullLink"></span>
                            </a>
                        </div>

                        <button className="btn btn-primary btn-actualites">En savoir plus</button>
                    </div>
                </div>
            </div>
        );
    }
}