/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable no-useless-constructor */
import Moment from 'react-moment';
import 'moment/locale/fr';
import { BaseComponent } from '../_base-component';
import './VignettesComponent.scss';
import { Link, Route } from 'react-router-dom';
import { Routes } from '../../constantes/routes';

export class VignetteConcertComponent extends BaseComponent {
    constructor() {
        super();
    }

    handleClick() {
        setTimeout(() => {
            window.location.reload();
        }, 100)
    }

    render() {
        let concert = this.props.concert;
        console.log("\n \n ~ file: VignetteConcertComponent.js ~ line 15 ~ VignetteConcertComponent ~ render ~ this.props", this.props)
        if (!concert) {
            concert = {
                "id": 5,
                "name": "Rock or Bust World Tour",
                "image": 'https://img.huffingtonpost.com/asset/5f1a00b8270000b10fe674d7.jpeg?cache=vM0RbxN9YX&ops=scalefit_630_noupscale',
                "musicGroup": {
                    "name": "AC/DC",
                },
                "date": "2021-05-22T20:00:00+02:00",
                "musicCategory": {
                    "value": "Rock"
                },
                "concertHall": {
                    "name": "Le Grand Théâtre",
                    "location": {
                        "name": "Aix-en-Provence"
                    }
                },
            };
        }
        return (
            <div className="card-container">
                <div className="card card--concert shadow">
                    <div className="row g-0">
                        <div className="col-md-12 card-header-img">
                            <img src={concert.image} alt="" />
                        </div>
                        <div className="col-md-12 d-flex justify-content-center">
                            <div className="card-body">
                                <h5 className="card-title">{concert.musicGroup.name} {concert.name}</h5>
                                <p className="card-text">
                                    <Moment format="D MMMM YYYY à HH:mm" locale="fr">{concert.date}</Moment></p>
                                <p className="card-text">{concert.concertHall.location.name}</p>
                                <p className="card-text"><small className="text-muted">{concert.musicCategory.value}</small></p>
                                <span>Tarifs : de {concert.minPrice}€ à {concert.maxPrice}€</span>
                            </div>
                            <Link to={{
                                pathname: Routes.DetailConcert + '/' + concert.id
                            }} onClick={this.handleClick}>
                                <span className="fullLink"></span>
                            </Link>
                        </div>

                    </div>
                </div>
                <Link className="btn btn-primary btn-reserve" to={{ pathname: Routes.ReservationConcert + '/' + concert.id }}>Réserver</Link>
            </div>
        );
    }
}