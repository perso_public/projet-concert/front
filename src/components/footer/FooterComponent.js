/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable no-useless-constructor */
import { BaseComponent } from '../_base-component';
import './FooterComponent.scss';
import { Routes } from '../../constantes/routes';
import { Link } from 'react-router-dom';
import { Col, Toast, Row } from 'react-bootstrap';


export class FooterComponent extends BaseComponent {
    constructor() {
        super();
        this.state = {
            show: false,
            setShow: false,
        }
    }

    render() {
        return (
            <div>
                <footer>
                    <div>
                        <Row>
                            <Col xs={6}>
                                <Toast className="toast" onClose={() => this.setState({ show: false })} show={this.state.show} delay={3000} autohide>
                                    <Toast.Body>Votre adresse e-mail a bien été enregistrée.</Toast.Body>
                                </Toast>
                            </Col>
                        </Row>
                    </div>
                    <div className="footer_header-container row">
                        <div className="socialMedia col-lg-6 col-12">
                            <span className="h3">Réseaux sociaux</span>
                            <span>Suivez-nous sur nos réseaux sociaux pour vous tenir informé des concerts à venir</span>
                            <div className="socialMedia-icons mt-3">
                                <a href="#"><i className="bi bi-facebook"></i></a>
                                <a href="#"><i className="bi bi-twitter"></i></a>
                                <a href="#"><i className="bi bi-instagram"></i></a>
                                <a href="#"><i className="bi bi-youtube"></i></a>
                            </div>
                        </div>

                        <div className="newsLetter col-lg-6 col-12">
                            <span className="h3">Newsletter</span>
                            <span>Recevez notre newsletter pour ne manquer aucune informations</span>
                            <div className="input-group mb-3 mt-3">
                                <input type="text" className="form-control" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="basic-addon2" />
                                <button onClick={() => this.setState({ show: true })} className="btn btn-primary" type="button" id="button-addon2">Valider</button>
                            </div>
                        </div>
                    </div>

                    <div className="footer_content-container">
                        <div>
                            <span className="text-uppercase">programmation</span>
                            <a href={Routes.Programmation}> Tous les évènements</a>
                            <Link to={{ pathname: Routes.Programmation, data: { location: 'Aix-en-Provence' } }}>Aix-en-Provence</Link>
                            <Link to={{ pathname: Routes.Programmation, data: { location: 'Bourges' } }}>Bourges</Link>
                            <Link to={{ pathname: Routes.Programmation, data: { location: 'Cannes' } }}>Cannes</Link>
                            <Link to={{ pathname: Routes.Programmation, data: { location: 'Dunkerque' } }}>Dunkerque</Link>
                            <Link to={{ pathname: Routes.Programmation, data: { location: 'Echirolles' } }}>Echirolles</Link>
                        </div>

                        <hr />

                        <div>
                            <div>
                                <span className="text-uppercase">restauration</span>
                                <a href="#">Présentation</a>
                                <a href="#">Réserver</a>
                            </div>
                            <div>
                                <span className="text-uppercase">parking</span>
                                <a href="#">Présentation</a>
                                <a href="#">Réserver</a>
                            </div>
                        </div>

                        <hr />

                        <div>
                            <div>
                                <span className="text-uppercase">privatisation</span>
                                <a href={Routes.PrivatisationPage}>Présentation</a>
                                <a href={Routes.PrivatisationReservationPage}>Réserver</a>
                            </div>
                            <div>
                                <a href="#" className="text-uppercase">actualités</a>

                            </div>
                        </div>

                        <hr />

                        <div>
                            <div>
                                <span className="text-uppercase">infos pratiques</span>
                                <a href="#">Comment venir ?</a>
                                <a href="#">FAQ</a>
                            </div>

                            <div>
                                <a href="#" className="text-uppercase">contact</a>
                            </div>
                        </div>

                        <hr />

                        <div>
                            <a href="#">POLITIQUES</a>
                            <a href="#">CGU</a>
                            <a href="#">Mentions légales</a>
                        </div>
                    </div>

                    <div className="copyright">
                        <span>&copy; 2021 Symfony Concert - Tous droits réservés</span>
                    </div>

                </footer>
            </div >
        );
    }
}