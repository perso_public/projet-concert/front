import './ReservationCoordonneesPage.scss';
import { NavbarComponent } from '../../components/navbar/NavbarComponent'
import { BasePage } from '../_base-page';
import { FooterComponent } from '../../components/footer/FooterComponent';
import { LoginComponent } from '../../components/Login-Register/login/LoginComponent';
import { RegisterComponent } from '../../components/Login-Register/register/RegisterComponent';
import { ReservationCoordonneesService } from "../../services/reservationCoordonnees.service.js";
import { LoginService } from '../../services/login.service';
import { RegisterService } from '../../services/register.service';
import { BreadCrumbComponent } from '../../components/breadcrump/BreadCrumbComponent';
import { Redirect } from "react-router-dom";
import { Routes } from '../../constantes/routes';

class ReservationCoordonneesPage extends BasePage {

    constructor(props) {
        super(props);
        const user = JSON.parse(localStorage.getItem('user'));
        const message = "";
        // localStorage.removeItem('user');
        if (user) {
            this.state = {
                id: user.id,
                lastName: user.lastName,
                firstName: user.firstName,
                email: user.email,
                emailConfirmation: user.emailConfirmation,
                password: '',
                passwordConfirmation: '',
                phone: user.phone,
                birthdate: user.birthdate,
                gender: user.gender,
                line1: user.address.line1,
                building: user.address.building,
                namedPlace: user.address.namedPlace,
                postalCode: user.address.postalCode,
                city: user.address.city,
                country: user.address.country,
                newAccount: "undefined",
                error: '',
            };
        } else {
            this.state = {
                id: '',
                lastName: '',
                firstName: '',
                email: '',
                emailConfirmation: '',
                password: '',
                passwordConfirmation: '',
                phone: '',
                birthdate: '',
                gender: '',
                line1: '',
                building: '',
                namedPlace: '',
                postalCode: '',
                city: '',
                country: '',
                newAccount: undefined,
                step: 3,
            };
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.login = this.login.bind(this);
        this.register = this.register.bind(this);
    }

    handleChange(e) {
        const name = e.target.name
        this.setState({
            [name]: e.target.value
        })
    }

    async handleSubmit(e) {
        e.preventDefault()
        await this.init(this.state);
    }

    handleClick = (button) => {
        this.setState(({ newAccount: button }))
    }

    async init(data) {
        const response = await ReservationCoordonneesService.updateUserData(data);
        this.users = response.data;
        this.setState(this.users);
        const user = this.state;
        localStorage.setItem('user', JSON.stringify(user));
        const route = Routes.Cart + '/' + this.concertId + Routes.PaymentPage;
        this.setState({redirect: route});
    }

    async login(e) {
        e.preventDefault()
        console.log(this.state)
        const response = await LoginService.getlogin(this.state);
        console.log(response);
        this.users = response.data;
        if (this.users['status'] == 400){
            this.setState({error: <div class="alert alert-danger" role="alert">Connexion impossible, mauvaise combinaison</div>});
        }
        else {
            this.setState(this.users);
            const user = this.state;
            localStorage.setItem('user', JSON.stringify(user));
            if (user) {
                this.setState(({newAccount: false}));
            }
        }
    }

    async register(e) {
        e.preventDefault()
        console.log(this.state)
        const response = await RegisterService.postRegister(this.state);
        this.users = response.data;
        if (this.users["status"] == 404){
            return this.message = this.users["message"];
        }
        /*if (this.users["detail"]){
            this.users["violations"];
        }*/
        this.setState(this.users);
        const user = this.state;
        localStorage.setItem('user', JSON.stringify(user));
        const route = Routes.Cart + '/' + this.concertId + Routes.PaymentPage;
        this.setState({redirect: route});
    }

    render() {
        if(this.state.redirect) {
            console.log(this.state.redirect);
            return <Redirect to={this.state.redirect} /> 
        }
        const { match: { params } } = this.props;
        this.concertId = params.concertId;

        //console.log(localStorage.getItem('user'));
        if (this.state.newAccount === true) {
            return (
                <div>
                    <NavbarComponent />

                    <div className="ReservationConcert">

                        <BreadCrumbComponent selectedStep={this.state.step}></BreadCrumbComponent>
                        <h1>Création de votre compte</h1>
                        <form className="container register-page__form" onSubmit={this.register}>
                            <div>
                                <label>
                                    Email *
                                </label>
                                <input name="email" type="text" value={this.state.email} onChange={this.handleChange} />
                            </div>
                            <div>
                                <label>
                                    Confirmation e-mail *
                                </label>
                                <input name="emailConfirmation" type="text" value={this.state.emailConfirmation} onChange={this.handleChange} />
                            </div>
                            <div>
                                <label>
                                    Mot de passe *
                                </label>
                                <input name="password" type="password" value={this.state.password} onChange={this.handleChange} />
                                <label>
                                    Votre mot de passe doit comprendre au moins 8 caractères, une lettre majuscule, une lettre minusculte et un chiffre (0-9)
                                </label>
                            </div>
                            <div>
                                <label>
                                    Confirmation mot de passe *
                                </label>
                                <input name="passwordConfirmation" type="password" value={this.state.passwordConfirmation} onChange={this.handleChange} />
                            </div>
                            <div>
                                <label>
                                    Civilité *
                                </label>
                                <select name="gender" value={this.state.gender} onChange={this.handleChange}>
                                    <option value="M">Homme</option>
                                    <option value="F">Femme</option>
                                    <option value="A">Autres</option>
                                </select>
                            </div>
                            <div>
                                <label>
                                    Nom *
                                </label>
                                <input name="lastName" type="text" value={this.state.lastName} onChange={this.handleChange} />
                            </div>

                            <div>
                                <label>
                                    Prénom *
                                </label>
                                <input name="firstName" type="text" value={this.state.firstName} onChange={this.handleChange} />
                            </div>
                            <div>
                                <label>
                                    N° et libellé de la voie *
                                </label>
                                <input name="line1" type="text" value={this.state.line1} onChange={this.handleChange} />
                            </div>
                            <div>
                                <label>
                                    Immeuble, Bâtiment, Résidence
                                </label>
                                <input name="building" type="text" value={this.state.building} onChange={this.handleChange} />
                            </div>
                            <div>
                                <label>
                                    Lieu-dit, boîte postale, etc
                                </label>
                                <input name="namedPlace" type="text" value={this.state.namedPlace} onChange={this.handleChange} />
                            </div>
                            <div>
                                <label>
                                    Code postal *
                                </label>
                                <input name="postalCode" type="number" value={this.state.postalCode} onChange={this.handleChange} />
                            </div>
                            <div>
                                <label>
                                    Ville *
                                </label>
                                <input name="city" type="text" value={this.state.city} onChange={this.handleChange} />
                            </div>
                            <div>
                                <label>
                                    Pays *
                                </label>
                                <input name="country" type="text" value={this.state.country} onChange={this.handleChange} />
                            </div>
                            <div>
                                <label>
                                    Téléphone *
                                </label>
                                <input name="phone" type="number" value={this.state.phone} onChange={this.handleChange} />
                            </div>

                            <div>
                                <label>
                                    Date de naissance *
                                </label>
                                <input name="birthdate" type="date" value={this.state.birthdate} onChange={this.handleChange} />
                            </div>
                            <a className="btn btn-secondary" onClick={() => this.handleClick(false)}>Annuler</a>
                            <button className="btn btn-primary" type="submit">Valider</button>
                        </form>
                    </div>
                    <FooterComponent></FooterComponent>
                </div>
            )
        }
        if (this.state.newAccount === false || localStorage.getItem('user')) {
            return (
                <div>
                    <NavbarComponent />

                    <div className="ReservationConcert">

                        <BreadCrumbComponent selectedStep={this.state.step}></BreadCrumbComponent>

                        <h1 className="">VERIFIEZ VOS INFORMATIONS PERSONNELLES</h1>
                        <form className="container register-page__form" onSubmit={this.handleSubmit}>
                            <div>
                                <label>
                                    Email *
                                </label>
                                <input name="email" type="text" value={this.state.email} onChange={this.handleChange} />
                            </div>
                            <div>
                                <label>
                                    Civilité *
                                </label>
                                <select name="gender" value={this.state.gender} onChange={this.handleChange}>
                                    <option value="M">Homme</option>
                                    <option value="F">Femme</option>
                                    <option value="A">Autres</option>
                                </select>
                            </div>
                            <div>
                                <label>
                                    Nom *
                                </label>
                                <input name="lastName" type="text" value={this.state.lastName} onChange={this.handleChange} />
                            </div>

                            <div>
                                <label>
                                    Prénom *
                                </label>
                                <input name="firstName" type="text" value={this.state.firstName} onChange={this.handleChange} />
                            </div>
                            <div>
                                <label>
                                    N° et libellé de la voie *
                                </label>
                                <input name="line1" type="text" value={this.state.line1} onChange={this.handleChange} />
                            </div>
                            <div>
                                <label>
                                    Immeuble, Bâtiment, Résidence
                                </label>
                                <input name="building" type="text" value={this.state.building} onChange={this.handleChange} />
                            </div>
                            <div>
                                <label>
                                    Lieu-dit, boîte postale, etc
                                </label>
                                <input name="namedPlace" type="text" value={this.state.namedPlace} onChange={this.handleChange} />
                            </div>
                            <div>
                                <label>
                                    Code postal *
                                </label>
                                <input name="postalCode" type="number" value={this.state.postalCode} onChange={this.handleChange} />
                            </div>
                            <div>
                                <label>
                                    Ville *
                                </label>
                                <input name="city" type="text" value={this.state.city} onChange={this.handleChange} />
                            </div>
                            <div>
                                <label>
                                    Pays *
                                </label>
                                <input name="country" type="text" value={this.state.country} onChange={this.handleChange} />
                            </div>
                            <div>
                                <label>
                                    Téléphone *
                                </label>
                                <input name="phone" type="number" value={this.state.phone} onChange={this.handleChange} />
                            </div>

                            <div>
                                <label>
                                    Date de naissance *
                                </label>
                                <input name="birthdate" type="date" value={this.state.birthdate} onChange={this.handleChange} />
                            </div>
                            <a className="btn btn-secondary" onClick={() => this.handleClick(undefined)}>Annuler</a>
                            <button className="btn btn-primary" type="submit">Valider</button>
                        </form>
                    </div>
                    <FooterComponent></FooterComponent>
                </div>
            );
        }

        return (
            <div>
                <NavbarComponent />
                <div className="ReservationConcert">

                    <BreadCrumbComponent selectedStep={this.state.step}></BreadCrumbComponent>


                    <div className="row reservation-coordonnees__content">
                        <LoginComponent></LoginComponent>
                        <div className="login-component col-12 col-sm-5">
                            <div className="login-component__header">
                                <span>Vous avez déjà un compte</span> <br />
                                    Connectez vous
                        </div>
                            <form className="container login-page__form" onSubmit={this.login}>
                                {this.state.error}
                                <div>
                                    <label htmlFor="email" >Votre adresse e-mail</label>
                                    <input name="email" type="text" value={this.state.email} onChange={this.handleChange} />
                                </div>

                                <div>
                                    <label htmlFor="password">Votre mot de passe</label>
                                    <input name="password" type="password" value={this.state.password} onChange={this.handleChange} />
                                </div>

                                <div>
                                    <button className="btn btn-primary" type="submit">Valider</button>
                                    <button className="btn btn-primary btn-primary--pwd" href="#">Mot de passe oublié</button>
                                </div>
                            </form>
                        </div>
                        <div className="col-sm-1 col-12"></div>
                        <div className="register-component col-12 col-sm-6">
                            <div className="register-component__header">
                                <span>Vous n'avez pas de compte</span> <br />
                                    Créez votre compte
                                </div>

                            <div>
                                <p>Créer votre compte et simplifiez vos réservations.
                                Conservez vos données en toute sécurité et évitez de remplir vos informations à chaque commande.
                                    Gérez vos alertes e-mails pour vos artistes ou salles préférés. <br />
                                    Téléchargez et imprimez vos E-Tickets et factures d'achat directement depuis votre compte.
                                </p>
                            </div>

                            <div>
                                <button className="btn btn-primary" onClick={() => this.handleClick(true)}>Créer mon compte</button>
                            </div>
                        </div>
                        <RegisterComponent></RegisterComponent>
                    </div>


                </div>

                <FooterComponent></FooterComponent>
            </div>
        );
    }
}

export default ReservationCoordonneesPage;