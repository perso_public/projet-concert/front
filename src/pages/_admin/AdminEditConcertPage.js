import './AdminEditConcertPage.scss';
import { NavbarComponent } from '../../components/navbar/NavbarComponent'
import { BasePage } from '../_base-page';
import { ConcertService } from '../../services/concert.service';
import { LoaderComponent } from '../../components/loader/LoaderComponent';
import { Link } from 'react-router-dom';
import { Routes } from '../../constantes/routes';
import { ConcertHallService } from '../../services/concert-hall.service';
import { RefValueService } from '../../services/refValue.service';
import { Editor } from "react-draft-wysiwyg";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import { EditorState, convertToRaw } from 'draft-js';
import draftToHtml from 'draftjs-to-html';
import { Alert } from 'react-bootstrap';
import { FooterComponent } from '../../components/footer/FooterComponent';

class AdminEditConcertPage extends BasePage {
    concertId;
    state;

    concertImageInput;
    musicGroupNameInput;
    selectedMusicCategoryInput;
    concertNameInput;
    concertDateInput;
    concertHourInput;
    concertOpeningHoursInput;
    selectedConcertHallInput;
    maxPriceInput;
    decreasingPercentageInput;

    constructor() {
        super();
        this.state = {
            loading: false,
            isEditionMode: false,
            concertHalls: [],
            selectedConcertHallId: undefined,
            musicCategories: [],
            selectedMusicCategoryId: undefined,
            maxPrice: undefined,
            minPrice: undefined,
            decreasingPercentage: undefined,
            calculatedPrices: [],
            ticketCategories: [],
            musicGroupName: undefined,
            concertName: undefined,
            musicGroupPresentation: undefined,
            musicGroupImage: 'https://www.coved.com/wp-content/uploads/2016/11/orionthemes-placeholder-image-1.png',
            editorState: EditorState.createEmpty(),
            concertDate: undefined,
            concertOpeningHours: undefined,
            concertDateHours: undefined,
            creationSuccess: false,
        };
    }

    async componentDidMount() {
        this.setState({ loading: true });

        if (this.concertId !== 'new') {
            const concertResponse = await ConcertService.getConcert(this.concertId);
            console.log("\n \n ~ concertResponse", concertResponse);
            if (concertResponse?.data) {
                const concert = concertResponse.data;
                this.setState({
                    selectedConcertHallId: concert.concertHall.id,
                    selectedMusicCategoryId: concert.musicCategory.id,
                    maxPrice: concert.maxPrice,
                    decreasingPercentage: concert.decreasingPercentage,
                    musicGroupName: concert.musicGroup.name,
                    musicGroupPresentation: concert.musicGroup.description,
                    musicGroupImage: concert.musicGroup.image,
                    concertDate: new Date(concert.date),
                    concertOpeningHours: new Date(concert.openingHours),
                    concertDateHours: new Date(concert.date),
                    concertName: concert.name,
                    isEditionMode: true,
                });
            }
        } else {
            this.setState({ isEditionMode: false });
        }

        const concertHallsResponse = await ConcertHallService.getAllConcertHalls();
        console.log("\n \n ~ concertHallsResponse", concertHallsResponse)
        if (concertHallsResponse?.data) {
            this.setState({ concertHalls: concertHallsResponse.data });
        }

        const musicCategoryResponse = await RefValueService.getAllOfTypeCode("MusicCategoryCode");
        console.log("\n \n ~ musicCategoryResponse", musicCategoryResponse)
        if (musicCategoryResponse?.data) {
            this.setState({ musicCategories: musicCategoryResponse.data });
        }

        const ticketCategoriesResponse = await RefValueService.getAllOfTypeCode("ConcertTicketCategoryCode");
        console.log("\n \n ~ ticketCategoriesResponse", ticketCategoriesResponse)
        if (ticketCategoriesResponse?.data) {
            this.setState({ ticketCategories: ticketCategoriesResponse.data });
        }

        this.setState({ loading: false });
    }

    clearInputs() {
        setTimeout(() => {
            this.setState({
                selectedConcertHallId: undefined,
                selectedMusicCategoryId: undefined,
                maxPrice: undefined,
                decreasingPercentage: undefined,
                calculatedPrices: [],
                musicGroupName: undefined,
                concertName: undefined,
                musicGroupPresentation: undefined,
                musicGroupImage: undefined,
                editorState: EditorState.createEmpty(),
                concertDate: undefined,
                concertOpeningHours: undefined,
                concertDateHours: undefined,
                creationSuccess: false,
            });
        }, 100);

        setTimeout(() => {
            this.concertImageInput.value = '';
            this.musicGroupNameInput.value = '';
            this.selectedMusicCategoryInput.value = '';
            this.concertNameInput.value = '';
            this.concertDateInput.value = '';
            this.concertHourInput.value = '';
            this.concertOpeningHoursInput.value = '';
            this.selectedConcertHallInput.value = '';
            this.maxPriceInput.value = '';
            this.decreasingPercentageInput.value = '';
        }, 200);

        console.log('cleared');
    }

    handleChange(event) {
        let target;
        let value;
        let name;

        if (event.blocks) {
            setTimeout(() => {
                value = draftToHtml(convertToRaw(this.state.editorState.getCurrentContent()));
                this.setState({ musicGroupPresentation: value });
            }, 100);
        } else {
            target = event.target;
            value = target.value;
            name = target.name;

            this.setState({ [name]: value });
        }

        setTimeout(() => {
            this.calculatePrices();
        }, 100);
    }

    calculatePrices() {
        if (!this.state.maxPrice || !this.state.decreasingPercentage) {
            this.setState({ calculatedPrices: [] });
            return;
        }

        const prices = [];
        const rowNbPerCategory = 3;
        let currentMaxPrice = parseInt(this.state.maxPrice.toString(), 10);
        let currentMinPrice = this.state.maxPrice;
        let cpt = 1;
        let categoriesCpt = 1;
        for (const item of this.state.ticketCategories) {
            for (let j = 0; j < rowNbPerCategory; j++) {
                if (categoriesCpt === 1 && j === 0) {
                    currentMinPrice = this.state.maxPrice;
                } else {
                    currentMinPrice = (this.state.maxPrice * (100 - (this.state.decreasingPercentage * cpt)) / 100);
                    cpt++;
                }
            }

            prices.push({ category: item, min: currentMinPrice, max: currentMaxPrice });
            currentMaxPrice = (this.state.maxPrice * (100 - (this.state.decreasingPercentage * cpt)) / 100);
            categoriesCpt++;
        }

        this.setState({ calculatedPrices: prices, minPrice: currentMinPrice });
    }

    async validate(state) {
        try {
            this.setState({ loading: true });

            if (!state.selectedConcertHallId || !state.selectedMusicCategoryId || !state.concertName || !state.musicGroupPresentation ||
                !state.musicGroupName || !state.musicGroupImage || !state.concertDateHours || !state.concertOpeningHours || !state.calculatedPrices ||
                !state.musicGroupImage || !state.minPrice || !state.maxPrice || !state.decreasingPercentage || !state.concertDate) {
                alert('Erreur : veuillez vérifier votre saisie');
                this.setState({ loading: false });
                return;
            }

            const concertTickets = [];
            for (const item of state.calculatedPrices) {
                concertTickets.push({
                    concert_id: 1,
                    category: '/api/ref_values/' + item.category.id,
                    price: item.max,
                });
            }

            const concert = {
                concertHall: '/api/concert_halls/' + state.selectedConcertHallId,
                musicCategory: '/api/ref_values/' + state.selectedMusicCategoryId,
                name: state.concertName,
                musicGroup: {
                    description: state.musicGroupPresentation,
                    name: state.musicGroupName,
                    image: state.musicGroupImage,
                },
                concertTickets: concertTickets,
                date: new Date(state.concertDate + ' ' + state.concertDateHours),
                openingHours: new Date(state.concertDate + ' ' + state.concertOpeningHours),
                isDeleted: false,
                image: state.musicGroupImage,
                minPrice: parseInt(state.minPrice.toString(), 10),
                maxPrice: parseInt(state.maxPrice.toString(), 10),
                decreasingPercentage: parseInt(state.decreasingPercentage.toString(), 10),
            };

            console.log("\n \n ~ concert", JSON.stringify(concert));

            if (this.concertId !== 'new') {
                concert.id = parseInt(this.concertId.toString(), 10);
                await ConcertService.modifyConcert(this.concertId, concert);

            } else {
                await ConcertService.createConcert(concert);
            }

            setTimeout(() => {
                this.setState({ creationSuccess: true, loading: false });
            }, 100);
        } catch (err) {
            alert(err);
            this.setState({ loading: false });
            console.log(err);
        }

        window.scrollTo(0, 0);
    }

    onEditorStateChange = (editorState) => {
        this.setState({
            editorState,
        });
    };

    render() {
        const { match: { params } } = this.props;
        this.concertId = params.id;

        return (
            <div>
                {this.state.loading ? <LoaderComponent></LoaderComponent> : null}

                <NavbarComponent />

                <div className="AdminEditConcert">
                    <div className="header">
                        <h1>BONJOUR ADMINISTRATEUR - {this.state.isEditionMode ? 'MODIFIER' : 'AJOUTER'} UN CONCERT</h1>
                    </div>

                    <div className="container">
                        {this.state.creationSuccess ? <div className="row">
                            <div className="alert alert-success" role="alert">
                                Concert {this.state.isEditionMode ? 'modifié ' : 'créé '} !
                            </div>
                        </div> : null}

                        <div className="first row">
                            <div className="picture col-sm-12 col-md-4" style={{ backgroundImage: 'url(' + this.state.musicGroupImage + ')' }}></div>
                            <div className="col-sm-12 col-md-8">
                                <div className="item">
                                    <label>Image :</label>
                                    <input name="musicGroupImage" type="text" value={this.state.musicGroupImage} onChange={(event) => this.handleChange(event)} ref={(el) => (this.concertImageInput = el)} />
                                </div>

                                <div className="item">
                                    <label>Nom de l'artiste / groupe :</label>
                                    <input name="musicGroupName" type="text" value={this.state.musicGroupName} onChange={(event) => this.handleChange(event)} ref={(el) => (this.musicGroupNameInput = el)} />
                                </div>

                                <div className="item">
                                    <label>Type de musique :</label>
                                    <select name="selectedMusicCategoryId" id="musicCategories" value={this.state.selectedMusicCategoryId} onChange={(event) => this.handleChange(event)} ref={(el) => (this.selectedMusicCategoryInput = el)}>
                                        <option value={undefined}>{undefined}</option>

                                        {this.state.musicCategories ? this.state.musicCategories.map(x => (
                                            <option key={x.code} value={x.id}>{x.value}</option>
                                        )) : ''}
                                    </select>
                                </div>

                                <div className="item">
                                    <label>Nom du concert :</label>
                                    <input name="concertName" type="text" value={this.state.concertName} onChange={(event) => this.handleChange(event)} ref={(el) => (this.concertNameInput = el)} />
                                </div>

                                <div className="item">
                                    <label>Date :</label>
                                    <input name="concertDate" type="date" value={this.state.concertDate} onChange={(event) => this.handleChange(event)} ref={(el) => (this.concertDateInput = el)} />
                                </div>

                                <div className="item">
                                    <label>Heure :</label>
                                    <input name="concertDateHours" type="time" value={this.state.concertDateHours} onChange={(event) => this.handleChange(event)} ref={(el) => (this.concertHourInput = el)} />
                                </div>

                                <div className="item">
                                    <label>Heures d'ouverture :</label>
                                    <input name="concertOpeningHours" type="time" value={this.state.concertOpeningHours} onChange={(event) => this.handleChange(event)} ref={(el) => (this.concertOpeningHoursInput = el)} />
                                </div>

                                <div className="item">
                                    <label>Lieu (ville) :</label>
                                    <select name="selectedConcertHallId" id="concertHalls" value={this.state.selectedConcertHallId} onChange={(event) => this.handleChange(event)} ref={(el) => (this.selectedConcertHallInput = el)}>
                                        <option value={undefined}>{undefined}</option>

                                        {this.state.concertHalls ? this.state.concertHalls.map(x => (
                                            <option key={x.name} value={x.id}>{x.name} ({x.location.name})</option>
                                        )) : ''}
                                    </select>
                                </div>

                                <div className="item">
                                    <label>Tarif MAX :</label>
                                    <input name="maxPrice" type="number" min="0" value={this.state.maxPrice} onChange={(event) => this.handleChange(event)} ref={(el) => (this.maxPriceInput = el)} />
                                </div>

                                <div className="item">
                                    <label>Pourcentage tarif dégressif :</label>
                                    <input name="decreasingPercentage" type="number" min="0" max="100" value={this.state.decreasingPercentage} onChange={(event) => this.handleChange(event)} ref={(el) => (this.decreasingPercentageInput = el)} />
                                </div>

                                <div className="item">
                                    <label>Tranches de tarifs :</label>
                                    <div>
                                        {this.state.calculatedPrices?.length ? this.state.calculatedPrices.map(x => (
                                            <label key={x.category.value}>&nbsp;{x.category.value} : {x.min}€ à {x.max}€</label>
                                        )) : <Alert variant="danger">Saisir un tarif max et un pourcentage dégressif</Alert>}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="second">
                            <h2>Présentation de l'artiste / groupe</h2>

                            <Editor
                                editorState={this.state.editorState}
                                toolbarClassName="toolbarClassName"
                                wrapperClassName="wrapperClassName"
                                editorClassName="editorClassName"
                                onEditorStateChange={this.onEditorStateChange}
                                placeholder="Présentation de l'artiste / groupe ..."
                                value={this.state.musicGroupPresentation}
                                onChange={(event) => this.handleChange(event)}
                            />
                        </div>
                    </div>

                    <div className="actions">
                        <Link className="btn btn-danger" to={{ pathname: Routes.AdminListConcerts }}>ANNULER</Link>
                        <button className="btn btn-danger" onClick={() => this.clearInputs()}>EFFACER</button>
                        <button className="btn btn-primary" onClick={() => this.validate(this.state)}>
                            {this.state.isEditionMode ? 'MODIFIER ' : 'CRÉER '}LE CONCERT
                            </button>
                    </div>
                </div>

                <FooterComponent></FooterComponent>
            </div>
        );
    }
}

export default AdminEditConcertPage;