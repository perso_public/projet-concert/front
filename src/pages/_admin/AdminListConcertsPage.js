import './AdminListConcertsPage.scss';
import { NavbarComponent } from '../../components/navbar/NavbarComponent'
import { BasePage } from '../_base-page';
import { ConcertService } from '../../services/concert.service';
import { LoaderComponent } from '../../components/loader/LoaderComponent';
import { Link } from 'react-router-dom';
import { Routes } from '../../constantes/routes';
import Moment from 'react-moment';

class AdminListConcertsPage extends BasePage {
    concertId;
    state;

    constructor() {
        super();
        this.state = {
            concerts: undefined,
            loaded: false,
            listConcerts: undefined,
            today: new Date(),
        };
    }

    async componentDidMount() {
        const concertResponse = await ConcertService.getAllConcerts();
        console.log("\n \n ~ concertResponse", concertResponse);
        if (concertResponse?.data) {
            this.setState({
                concerts: concertResponse.data,
                listConcerts: concertResponse.data.map((d) =>
                    <div className="concert-item row">
                        <div className="picture col-12 col-md-4" style={{ backgroundImage: 'url(' + d.image + ')' }}></div>
                        <div className="info col-12 col-md-8">
                            <p>{d.musicGroup.name}</p>
                            <p>{d.name}</p>
                            <p>
                                <Moment format="D MMMM YYYY à HH:mm" locale="fr">{d.date}</Moment>
                            </p>
                            <p>Ouverture : <Moment format="HH:mm" locale="fr">{d.openingHours}</Moment></p>
                            <p>{d.concertHall?.location?.name} ({d.concertHall?.name})</p>
                            <p>{d.musicCategory?.value}</p>
                            <p>de {d.minPrice} € à {d.maxPrice} €</p>
                            <p>Présentation de l'artiste : {d.musicGroup?.description ? 'Oui' : 'Non'}</p>

                            <Link className="btn btn-primary" to={{ pathname: Routes.AdminListConcerts + '/' + d.id }}>MODIFIER</Link>
                        </div>
                    </div>
                ),
            });
        }

        this.setState({ loaded: true });
    }

    render() {
        const { match: { params } } = this.props;
        this.concertId = params.id;

        return (
            <div>
                {!this.state.loaded ? <LoaderComponent></LoaderComponent> : null}

                <NavbarComponent />

                <div className="AdminListConcerts">
                    <div className="header">
                        <h1>BONJOUR ADMINISTRATEUR - CONCERTS</h1>
                    </div>

                    <div className="container">
                        <div className="container-header">
                            <p>
                                Aujourd'hui nous sommes le : &nbsp;
                                <Moment format="D MMMM YYYY" locale="fr">{this.state.today}</Moment>
                            </p>
                            <Link className="btn btn-primary" to={{ pathname: Routes.AdminListConcerts + '/new' }}>Ajouter un concert</Link>
                        </div>

                        <p>Les derniers concerts créés :</p>

                        {this.state.listConcerts}
                    </div>
                </div>
            </div>
        );
    }
}

export default AdminListConcertsPage;