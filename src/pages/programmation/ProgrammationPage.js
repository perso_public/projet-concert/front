import './ProgrammationPage.scss';
import { Link } from "react-router-dom";
import { BasePage } from '../_base-page';
import { Routes } from '../../constantes/routes';
import { CardComponent } from '../../components/card/CardComponent';
import { LocationService } from '../../services/location.service';
import { RefValueService } from '../../services/refValue.service';
import { ConcertService } from '../../services/concert.service';
import { NavbarComponent } from '../../components/navbar/NavbarComponent';
import { VignetteConcertComponent } from '../../components/vignettes/VignetteConcertComponent';
import { FooterComponent } from '../../components/footer/FooterComponent';
import React, { useState } from "react";
import Dropdown from 'react-bootstrap/Dropdown';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { LoaderComponent } from '../../components/loader/LoaderComponent';

const DatePickerComponent = ({ valueUpdated }) => {

    var date = null;

    const [startDate, setStartDate] = useState(date);
    const [endDate, setEndDate] = useState(date);

    const init = () => {
        const range = {
            'dateAfter': '',
            'dateBefore': '',
        };
        setStartDate(null);
        setEndDate(null);
        valueUpdated(range);
    }

    const onChangeStartDate = (date) => {
        const range = {
            'dateAfter': new Intl.DateTimeFormat('en-US').format(date),
        };
        if (endDate != null) {
            range['dateBefore'] = new Intl.DateTimeFormat('en-US').format(endDate);
        }
        else {
            range['dateBefore'] = '';
        }
        valueUpdated(range);
        setStartDate(date);
    }

    const onChangeEndDate = (date) => {
        const range = {
            'dateBefore': new Intl.DateTimeFormat('en-US').format(date),
        };
        if (startDate != null) {
            range['dateAfter'] = new Intl.DateTimeFormat('en-US').format(startDate);
        }
        else {
            range['dateAfter'] = '';
        }
        valueUpdated(range);
        setEndDate(date);
    }

    return (
        <>
            <div className="date-picker__range">
                <div>
                    <span>Du</span>
                    <DatePicker
                        dateFormat="dd/MM/yyyy"
                        selected={startDate}
                        onChange={date => onChangeStartDate(date)}
                        selectsStart
                        startDate={startDate}
                        endDate={endDate}

                    />
                    <i className="bi bi-calendar3"></i>
                </div>

                <div>
                    <span>Au</span>
                    <DatePicker
                        dateFormat="dd/MM/yyyy"
                        selected={endDate}
                        onChange={date => onChangeEndDate(date)}
                        selectsEnd
                        startDate={startDate}
                        endDate={endDate}
                        minDate={startDate}
                    />
                    <i className="bi bi-calendar3"></i>
                </div>
            </div>
            <button className="btn" onClick={init}>Réinitialiser les dates</button>
        </>
    );
};

class ProgrammationPage extends BasePage {

    constructor() {
        super();

        this.state = {
            locations: [],
            musicCategories: [],
            concerts: [],
            filterLocation: {'location': 'all'},
            filterMusicCategory: {'musicCategory': 'all'},
            filterRange: {},
            loaded: false,
        };
    }

    isActive(value) {
        return 'btn programmation-filters__btn '+((value===this.state.filterLocation.location) ?'active':'');
    }
    
    async getLocations() {
        let locationsHtml = [];
        locationsHtml.push(<button className={this.isActive('all')} onClick={() => this.setState({ filterLocation: {'location': 'all'}}, () => { this.getLocations(); this.getConcerts();})}>Toutes</button>);
        let resultLocation = await LocationService.getAllLocations();

        if (resultLocation.status !== 200) {
            console.log('erreur : ' + resultLocation.statusText);
        } else {
            for (const data of resultLocation.data) {
                locationsHtml.push(<button className={this.isActive(data.name)} onClick={() => this.setState({ filterLocation: {'location': data.name}}, () => { this.getLocations(); this.getConcerts();})}>{data.name}</button>);
            }
            this.setState({ locations: locationsHtml });
        }
    }

    async getMusicCategories() {
        let musicCategoriesHtml = [];
        musicCategoriesHtml.push(<div><input type="radio" name="musicCategory" value="toutes" defaultChecked onClick={() => this.setState({ filterMusicCategory: {'musicCategory': 'all'}}, this.getConcerts)}/><label htmlFor="toutes">toutes</label></div>);
        let resultCategory = await RefValueService.getAllOfTypeCode('MusicCategoryCode');

        if (resultCategory.status !== 200) {
            console.log('erreur : ' + resultCategory.statusText);
        } else {
            for (const data of resultCategory.data) {
                musicCategoriesHtml.push(<div><input type="radio" name="musicCategory" value="{data.value}" onClick={() => this.setState({ filterMusicCategory: {'musicCategory': data.value}}, this.getConcerts)} /><label htmlFor="{data.value}">{data.value}</label></div>);
            }
            this.setState({ musicCategories: musicCategoriesHtml });
        }
    }

    async getConcerts() {
        this.setState({ loaded: false });

        const filter = {...this.state.filterLocation, ...this.state.filterMusicCategory, ...this.state.filterRange};

        const order = [];
        let concertsHtml = [];
        let resultConcert = await ConcertService.getAllConcerts(filter, order);

        if (resultConcert.status !== 200) {
            console.log('erreur : ' + resultConcert.statusText);
        } else {
            for (const data of resultConcert.data) {
                const minPrice = ConcertService.getCategoriesMinPrice(data);
                const maxPrice = ConcertService.getCategoriesMaxPrice(data);
                // console.log(data);
                concertsHtml.push(<VignetteConcertComponent concert={data} minPrice={minPrice} maxPrice={maxPrice}></VignetteConcertComponent>);
            }
            this.setState({ concerts: concertsHtml, loaded: true });
        }
    }

    async componentDidMount() {
        const data = this.props.location.data;
        if(data && data.location) {
            this.setState({ filterLocation: {'location': data.location}}, this.getLocations)
        }
        else {
            //Affichage de la liste des lieux
            await this.getLocations();
        }       

        //Affichage de la liste des catégroies de musique
        await this.getMusicCategories();

        //Affichage de la liste des concerts
        await this.getConcerts();
    }

    render() {
        return (
            <div>
                {!this.state.loaded ? <LoaderComponent></LoaderComponent> : null}
                
                <NavbarComponent></NavbarComponent>

                <div className="programmationPage container">

                    <div className="main-title">
                        <h1>Programmation Page</h1>
                    </div>

                    <div className="programmation-filters">
                        <div className="programmation-filters__lieu">
                            <span>Lieu :</span>
                            <div>
                                {this.state.locations}
                            </div>
                        </div>
                        <div className="programmation-filters__categorie">
                            <span>Catégorie de musique :</span>
                            <div>
                                {this.state.musicCategories}
                            </div>
                        </div>

                        <div className="programmation-filters__date">
                            <span>Dates :</span>
                            <DatePickerComponent valueUpdated={ range => this.setState({ filterRange: range}, this.getConcerts)}></DatePickerComponent>
                        </div>

                        <div className="programmation-filters__trier">
                            {/* <span>Trier par :</span> */}
                            <Dropdown>
                                <Dropdown.Toggle variant="success" id="dropdown-basic">
                                    Trier par
                                </Dropdown.Toggle>

                                <Dropdown.Menu>
                                    <Dropdown.Item href="#/action-2">Date croissante</Dropdown.Item>
                                    <Dropdown.Item href="#/action-2">Date décroissante</Dropdown.Item>
                                    <Dropdown.Item href="#/action-2">A-Z</Dropdown.Item>
                                    <Dropdown.Item href="#/action-3">Z-A</Dropdown.Item>
                                </Dropdown.Menu>
                            </Dropdown>
                        </div>
                    </div>

                    <div className="programmation-content row">
                        {this.state.concerts}
                    </div>
                </div>

                <FooterComponent></FooterComponent>

            </div>

        );
    }
}

export default ProgrammationPage;