import './PaymentPage.scss';
import { NavbarComponent } from '../../components/navbar/NavbarComponent'
import { BasePage } from '../_base-page';
import { FooterComponent } from '../../components/footer/FooterComponent';
import VisaCard from '../../assets/visa-icon.svg'
import MasterCard from '../../assets/mastercard-icon.svg'
import ModalComponent from '../../components/modal/ModalComponent'
import { LoaderComponent } from '../../components/loader/LoaderComponent';
import { CartContentComponent } from '../../components/cart-content/CartContentComponent';
import { ReservationStepsComponent } from '../../components/reservation-steps/ReservationStepsComponent';
import { ConcertService } from '../../services/concert.service';
import { Routes } from '../../constantes/routes';
import { Redirect } from 'react-router-dom';
import { ConcertTicketReservationService } from '../../services/concertTicketReservation.service';
import { ConcertReservationService } from '../../services/concertReservation.service';
import { BreadCrumbComponent } from '../../components/breadcrump/BreadCrumbComponent';



class PaymentPage extends BasePage {

    state = {
        concert: null,
        cartHTML: [],
        ticketsHTML: [],
        loaded: false,
        loadedPayment: true,
        name: '',
        redirect: null,
        step: 4,
    }

    storage;


    constructor() {
        super();
        this.onSubmit = this.onSubmit.bind(this);

        this.storage = JSON.parse(localStorage.getItem('ticketsReservation'));
        console.log(this.storage);
    }

    async getConcert() {
        const resultConcert = await ConcertService.getConcert(this.concertId);
        console.log(resultConcert);
        if (resultConcert.status !== 200) {
            console.log('erreur : ' + resultConcert.statusText);
        } else {
            this.setState({ concert: resultConcert.data});
            const cartHTML = [];
            cartHTML.push(<CartContentComponent concert={this.state.concert} tickets={this.storage}></CartContentComponent>);
            this.setState({ cartHTML: cartHTML, loaded: true });

        }
    }

    async getTickets() {
        const ticketsHTML = [];
        for(const ticket of this.storage) {
            ticketsHTML.push(<div>Place {ticket.spanNumber}, Rang {ticket.rowNumber}</div>)
        }
        this.setState({ ticketsHTML: ticketsHTML});
    }

    async componentDidMount() {
        await this.getConcert();
        await this.getTickets();
        if(localStorage.getItem('user')) {
            this.setState({name: JSON.parse(localStorage.getItem('user')).firstName})
        }
    }

    async saveTickets() {
        const user = JSON.parse(localStorage.getItem('user'));
        const reservationNumber = Math.floor(Math.random() * 10000) + 1;
        const reservationNumberString = reservationNumber.toString();
        // localStorage.setItem('reservationNumber', reservationNumberString);

        const concertReservation = {
            email: user.email,
            gender: user.gender,
            lastname: user.lastName,
            firstname: user.firstName,
            line1: user.address.line1,
            building: user.address.building,
            namedPlace: user.address.namedPlace,
            postalCode: user.address.postalCode,
            city: user.address.city,
            country: user.address.country,
            phone: user.phone,
            birthday: user.birthdate,
            reservationNumber: reservationNumberString,
            purchaseDate: user.birthdate,
            // user: user
        }
        const concertTicketsReservation = [];

        for(const ticket of this.storage) {
            ticket.obtainingMethod = "/api/ref_values/" + ticket.obtainingMethod.id;
            ticket.concertTicket = "/api/concert_tickets/" + ticket.concertTicket.id;
            ticket.insurance = false;
            concertTicketsReservation.push(ticket);
        }
        concertReservation.concertTicketsReservation = concertTicketsReservation;

        const response = await ConcertReservationService.postConcertReservation(concertReservation);
        console.log('response');
        console.log(response);
        return response;
    }

    async onSubmit(event) {
        const route = Routes.Cart + "/" + this.concertId + "/" + "confirmation";
        event.preventDefault();
        this.setState({ loadedPayment: false });
        const response = await this.saveTickets();
        if (response.status == 200 || response.status == 201) {
            setTimeout(() => this.setState({loadedPayment: true, redirect: route}), 3000);
        }
        else {
            this.setState({loadedPayment: true});
            console.log(response.statusText);
        }
    }


    render() {
        if(this.state.redirect) {
            console.log(this.state.redirect);
            return <Redirect to={this.state.redirect} /> 
        }

        const { match: { params } } = this.props;
        this.concertId = params.concertId;

        return (
            <div className="position-relative">
                {!this.state.loaded ? <LoaderComponent></LoaderComponent> : null}
                <NavbarComponent />

                <div className="payment-page">
                    <BreadCrumbComponent selectedStep={this.state.step}></BreadCrumbComponent>

                    <div className="container">
                        <div>
                            <h2>Bonjour {this.state.name}</h2>
                        </div>
                        {this.state.cartHTML}

                        <div className="row my-3 py-3">
                            <p>
                                <u> Votre paiement</u>
                            </p>
                            <p> Veuillez saisir les informations de votre paiement: </p>

                            <form className="payment-page__form" id="form1" onSubmit={this.onSubmit}>

                                <div className="payment-page__form-paymentcards">
                                    <button className="btn"> <img src={VisaCard} alt="" /> </button>
                                    <button className="btn"> <img src={MasterCard} alt="" /> </button>
                                </div>

                                <label>
                                    Numéro de la carte :
                                        <input type="text" name="n_carte" required/>
                                </label>


                                <div>
                                    <label>
                                        Date d'expiration :
                                    </label>

                                    <label className="ps-3">
                                        Mois:
                                    </label>
                                    <input type="number" name="mois_expiration" min="01" max="12" required/>

                                    <label className="ps-3">
                                        Année:
                                    </label>
                                    <input type="number" name="annee_expiration" min="2021" max="2100" required/>
                                </div>

                                <div>
                                    <label>
                                        Cryptogramme visuel:
                                    </label>
                                    <input className="payment-page__form-crypto" type="text" name="cryptogramme_visuel" required/>
                                </div>
                            </form>
                        </div>

                        <div className="button-container">
                            {/* <a className="btn btn-danger" href="#"> Annuler</a> */}
                            <ModalComponent route={Routes.Cart + "/" + this.concertId}></ModalComponent>

                            <button type="submit" className="btn btn-primary" form="form1">Valider et payer</button>

                        </div>
                    </div>
                </div>

                {!this.state.loadedPayment ? <LoaderComponent></LoaderComponent> : null}


                <FooterComponent></FooterComponent>
            </div>
        );
    }
}

export default PaymentPage;