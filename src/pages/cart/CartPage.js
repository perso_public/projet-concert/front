import './CartPage.scss';
import { NavbarComponent } from '../../components/navbar/NavbarComponent'
import { BasePage } from '../_base-page';
import { FooterComponent } from '../../components/footer/FooterComponent';
import { ConcertService } from '../../services/concert.service';
import { CartContentComponent } from '../../components/cart-content/CartContentComponent';
import { LoaderComponent } from '../../components/loader/LoaderComponent';
import { BreadCrumbComponent } from '../../components/breadcrump/BreadCrumbComponent';
import { Link } from 'react-router-dom';
import { Routes } from '../../constantes/routes';



class CartPage extends BasePage {

    concertId;
    storage;
    constructor() {
        super();

        this.state = {
            concert: null,
            cartHTML: [],
            ticketsHTML: [],
            loaded: false,
            step: 2,
        }
        this.storage = JSON.parse(localStorage.getItem('ticketsReservation'));
        console.log(this.storage);
    }

    async getConcert() {
        const resultConcert = await ConcertService.getConcert(this.concertId);
        console.log(resultConcert);
        if (resultConcert.status !== 200) {
            console.log('erreur : ' + resultConcert.statusText);
        } else {
            this.setState({ concert: resultConcert.data});
            const cartHTML = [];
            cartHTML.push(<CartContentComponent concert={this.state.concert} tickets={this.storage} bin="true"></CartContentComponent>);
            this.setState({ cartHTML: cartHTML, loaded: true });

        }
    }

    async getTickets() {
        const ticketsHTML = [];
        for(const ticket of this.storage) {
            ticketsHTML.push(<div>Place {ticket.spanNumber}, Rang {ticket.rowNumber}</div>)
        }
        this.setState({ ticketsHTML: ticketsHTML});
    }

    async componentDidMount() {
        await this.getConcert();
        await this.getTickets();
    }

    render() {
        const { match: { params } } = this.props;
        this.concertId = params.concertId;
        localStorage.setItem('concertId', this.concertId);

        return (
            <div>
                {!this.state.loaded ? <LoaderComponent></LoaderComponent> : null}

                <NavbarComponent />

                <div className="cart">
                <BreadCrumbComponent selectedStep={this.state.step}></BreadCrumbComponent>

                    <div className="container ">
                        <div className="plan row">
                            <div className="col-8 plan__component">
                                plan
                            </div>

                            <div className="plan__chosen-places col-4">

                                <div>
                                    <div>
                                        {this.state.concert?.musicGroup?.name}
                                    </div>
                                    <div>
                                        {this.state.concert?.name}
                                    </div>
                                    <div>
                                        {this.state.concert?.concertHall?.location?.name}
                                    </div>
                                </div>

                                <div>
                                    {this.state.ticketsHTML}
                                </div>
                            </div>
                            {this.state.cartHTML}
                        </div>


                        <div className="button-container">
                            <Link className="btn btn-danger button" to={{ pathname: Routes.DetailConcert + '/' + this.concertId }}> ANNULER</Link>
                            <Link className="btn btn-primary button" to={{ pathname: Routes.Programmation }}> AUTRE COMMANDE</Link>
                            <Link className="btn btn-success button" to={{ pathname: Routes.ReservationCoordonnees + '/' + this.concertId }}> VALIDER</Link>
                        </div>


                    </div>
                </div>

                <FooterComponent></FooterComponent>
            </div>
        );
    }
}

export default CartPage;