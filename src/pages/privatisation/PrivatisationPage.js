import './PrivatisationPage.scss';
import { NavbarComponent } from '../../components/navbar/NavbarComponent'
import { BasePage } from '../_base-page';
import { FooterComponent } from '../../components/footer/FooterComponent';
import Carousel, { Dots, autoplayPlugin } from '@brainhubeu/react-carousel';
import '@brainhubeu/react-carousel/lib/style.css';
import { Routes } from '../../constantes/routes';

class PrivatisationPage extends BasePage {
    constructor() {
        super();
        this.state = {
            value: 0,
            slides: [
                (<img src={"../assets/imgs/salle-concert2-image.png"} />),
                (<img src={"../assets/imgs/salle-concert2-image.png"} />),
                (<img src={"../assets/imgs/salle-concert2-image.png"} />),
                (<img src={"../assets/imgs/salle-concert2-image.png"} />),
                (<img src={"../assets/imgs/salle-concert2-image.png"} />),
            ],
            thumbnails: [
                (<img src={'../assets/imgs/salle-concert2-thumbnail.png'} />),
                (<img src={'../assets/imgs/salle-concert2-thumbnail.png'} />),
                (<img src={'../assets/imgs/salle-concert2-thumbnail.png'} />),
                (<img src={'../assets/imgs/salle-concert2-thumbnail.png'} />),
                (<img src={'../assets/imgs/salle-concert2-thumbnail.png'} />),
            ],
        }
        this.onchange = this.onchange.bind(this);
    }

    onchange(value) {
        this.setState({ value });
    }

    render() {
        return (
            <div>
                <NavbarComponent />

                <div className="PrivatisationPage container">
                    <div className="main-title">
                        <h1>Privatisation - Présentation</h1>
                    </div>

                    <div classeName="thumbnail-img">
                        <Carousel value={this.state.value} slides={this.state.slides} onChange={this.onchange} animationSpeed={1000} plugins={[
                            'arrows',
                            'infinite',
                            {
                                resolve: autoplayPlugin,
                                options: {
                                    interval: 2000,
                                }
                            },
                        ]} />
                        <Dots number={this.state.thumbnails.length} thumbnails={this.state.thumbnails} value={this.state.value} onChange={this.onchange} number={this.state.slides.length} />
                    </div>

                    <div className="btn-center">
                        <p>
                            Vous avez toujours rêvé d'organiser des évènements dans l'un de nos lieux mythiques ? Nous vous mettons à
                            disposition l'une de nos salles pour réaliser vos évènements privés !
                            Pour réserver l'une de nos salles, cliquez sur le bouton ci-dessous et remplissez ce formulaire de pré-réservation.
                            Nous vous rappellerons afin d'échanger sur votre projet.
                        </p>

                        <p><a className="btn btn-primary" href={Routes.PrivatisationReservationPage}>Pré-réserver</a></p>
                    </div>
                </div>

                <FooterComponent></FooterComponent>
            </div>
        );
    }

}

export default PrivatisationPage;