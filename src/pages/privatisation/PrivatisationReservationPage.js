import './PrivatisationPage.scss';
import { NavbarComponent } from '../../components/navbar/NavbarComponent'
import { BasePage } from '../_base-page';
import { FooterComponent } from '../../components/footer/FooterComponent';
import '@brainhubeu/react-carousel/lib/style.css';

class PrivatisationReservationPage extends BasePage {
    constructor() {
        super();
    }
    render() {
        return (
            <div>
                <NavbarComponent />

                <div className="PrivatisationPage container main-title">
                    <h1>Privatisation - Réservation</h1>
                </div>

                <FooterComponent></FooterComponent>
            </div>
        );
    }

}

export default PrivatisationReservationPage;