import './App.scss';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import ExemplePage from '../exemple/ExemplePage';
import HomePage from '../home/HomePage';
import { Routes } from '../../constantes/routes';
import DetailConcertPage from "../detail-concert/DetailConcertPage";
import LoginPage from "../login/LoginPage";
import RegisterPage from "../register/RegisterPage";
import ProgrammationPage from "../programmation/ProgrammationPage";
import PrivatisationPage from "../privatisation/PrivatisationPage";
import PrivatisationReservationPage from "../privatisation/PrivatisationReservationPage";
import ReservationConcertPage from "../reservation-concert/ReservationConcertPage";
import ConfirmationPage from '../confirmation/ConfirmationPage';
import CartPage from '../cart/CartPage';
import PaymentPage from '../payment/PaymentPage';
import AdminListConcertsPage from '../_admin/AdminListConcertsPage';
import AdminEditConcertPage from '../_admin/AdminEditConcertPage';
import ReservationCoordonneesPage from '../reservation-coordonnees/ReservationCoordonneesPage';

function App() {
  return (
    <Router>
      <Switch className="container">
        <Route path={Routes.ExemplePage} component={ExemplePage} />
        <Route path={Routes.DetailConcert + '/:id'} component={DetailConcertPage} />
        <Route path={Routes.LoginPage} component={LoginPage} />
        <Route path={Routes.RegisterPage} component={RegisterPage} />
        <Route path={Routes.Programmation} component={ProgrammationPage} />
        <Route path={Routes.PrivatisationPage} component={PrivatisationPage} />
        <Route path={Routes.PrivatisationReservationPage} component={PrivatisationReservationPage} />
        <Route path={Routes.Cart + '/:concertId' + Routes.ConfirmationPage} component={ConfirmationPage} />
        <Route path={Routes.Cart + '/:concertId' + Routes.PaymentPage} component={PaymentPage} />
        <Route path={Routes.Cart + '/:concertId'} component={CartPage} />
        <Route path={Routes.AdminListConcerts + '/:id'} component={AdminEditConcertPage} />
        <Route path={Routes.AdminListConcerts} component={AdminListConcertsPage} />
        <Route path={Routes.ReservationConcert + '/:id'} component={ReservationConcertPage} />
        <Route path={Routes.ReservationCoordonnees + '/:concertId'} component={ReservationCoordonneesPage} />

        {/* Last page */}
        <Route path={Routes.Home} component={HomePage} />
      </Switch>
    </Router>
  );
}

export default App;
