import './ConfirmationPage.scss';
import { NavbarComponent } from '../../components/navbar/NavbarComponent'
import { BasePage } from '../_base-page';
import { FooterComponent } from '../../components/footer/FooterComponent';
import { Routes } from '../../constantes/routes';
import { CartContentComponent } from '../../components/cart-content/CartContentComponent';
import { ReservationStepsComponent } from '../../components/reservation-steps/ReservationStepsComponent';
import { ConcertService } from '../../services/concert.service';
import { LoaderComponent } from '../../components/loader/LoaderComponent';
import { BreadCrumbComponent } from '../../components/breadcrump/BreadCrumbComponent';


class ConfirmationPage extends BasePage {

    state = {
        concert: null,
        cartHTML: [],
        ticketsHTML: [],
        loaded: false,
        name: '',
        redirect: null,
        step: 5,
    }

    constructor() {
        super();
        this.storage = JSON.parse(localStorage.getItem('ticketsReservation'));
        console.log(this.storage);
        this.reservationNumber = localStorage.removeItem('reservationNumber');
    }

    async getConcert() {
        const resultConcert = await ConcertService.getConcert(this.concertId);
        console.log(resultConcert);
        if (resultConcert.status !== 200) {
            console.log('erreur : ' + resultConcert.statusText);
        } else {
            this.setState({ concert: resultConcert.data});
            const cartHTML = [];
            cartHTML.push(<CartContentComponent concert={this.state.concert} tickets={this.storage}></CartContentComponent>);
            this.setState({ cartHTML: cartHTML, loaded: true });

        }
    }

    async getTickets() {
        const ticketsHTML = [];
        for(const ticket of this.storage) {
            ticketsHTML.push(<div>Place {ticket.spanNumber}, Rang {ticket.rowNumber}</div>)
        }
        this.setState({ ticketsHTML: ticketsHTML});
    }

    async componentDidMount() {
        await this.getConcert();
        await this.getTickets();
        if(localStorage.getItem('user')) {
            this.setState({name: JSON.parse(localStorage.getItem('user')).firstName})
        }
    }

    render() {
        const { match: { params } } = this.props;
        this.concertId = params.concertId;

        return (
            <div>
                {!this.state.loaded ? <LoaderComponent></LoaderComponent> : null}
                <NavbarComponent />

                <div className="confirmation-page">
                    <BreadCrumbComponent selectedStep={this.state.step}></BreadCrumbComponent>

                    <div className="container">
                        <div>
                            <h3>Merci {this.state.name} pour votre achat !</h3>
                            <p>La référence de cette réservation est le {this.reservationNumber}.</p>
                            <p>Vous allez recevoir un e-mail de confirmation. Si vous avez choisi pour l'obtention des billets le mode "E-ticket" vous pouvez le télécharger
                                et l'imprimer depuis votre compte. </p>
                        </div>
                        {this.state.cartHTML}

                        <div className="button-container">
                            <a className="btn btn-primary" href={Routes.Programmation}> Faire une autre commande</a>
                            <a className="btn btn-secondary" href={Routes.Home}> Retour à la page d'accueil</a>
                        </div>
                    </div>
                </div>

                <FooterComponent></FooterComponent>
            </div>
        );
    }
}

export default ConfirmationPage;