import './HomePage.scss';
import { NavbarComponent } from '../../components/navbar/NavbarComponent'
import { Link } from "react-router-dom";
import { BasePage } from '../_base-page';
import { Routes } from '../../constantes/routes';
import { FooterComponent } from '../../components/footer/FooterComponent';
import { VignetteConcertComponent } from '../../components/vignettes/VignetteConcertComponent';
import { VignetteNewsComponent } from '../../components/vignettes/VignetteNewsComponent';
import { CarouselComponent } from '../../components/carousel/CarouselComponent';
import { ConcertService } from '../../services/concert.service';

class HomePage extends BasePage {

    state;

    constructor() {
        super();

        this.state = {
            loaded: false,
            concerts: [],
        };
    }

    async componentDidMount() {
        const concertsResponse = await ConcertService.getAllConcerts();
        console.log("\n \n ~ concertsResponse", concertsResponse)
        if (concertsResponse?.data) {
            concertsResponse.data.splice(6, concertsResponse.data.length);
            this.setState({ concerts: concertsResponse.data });
        }

        this.setState({ loaded: true });
    }

    render() {
        return (
            <div className="position-relative">
                <NavbarComponent />

                <div className="HomePage">
                    <CarouselComponent></CarouselComponent>
                    <div>
                        <p className="home-sous-titre">Prochainement dans nos salles</p>
                        <div className="row">
                            {this.state.concerts?.length ? this.state.concerts.map(x => (
                                <VignetteConcertComponent concert={x}></VignetteConcertComponent>
                            )) : ''}

                        </div>
                        <Link className="btn btn-primary" to={{ pathname: Routes.Programmation }}>Voir toute la programmation</Link>
                    </div>
                    <div>
                        <p className="home-sous-titre">Actualités</p>
                        <div className="row">
                            <VignetteNewsComponent></VignetteNewsComponent>
                            <VignetteNewsComponent></VignetteNewsComponent>
                            <VignetteNewsComponent></VignetteNewsComponent>
                            <VignetteNewsComponent></VignetteNewsComponent>
                        </div>
                        <p><button className="btn btn-primary">Voir toutes les actualités</button></p>
                    </div>
                    <div className="privatisation-section">
                        <p className="home-sous-titre">Des salles à votre disposition</p>
                        <div className="home-privatisation row">
                            <div className="col-lg-5 col-md-4">
                                <img src={'../assets/imgs/salle-concert2-image.png'} alt="" />
                            </div>

                            <div className="home-privatisation__texte col-lg-7 col-md-8">
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent tempor porta ipsum non lobortis.
                                    Maecenas nec leo quis tellus suscipit aliquam. Nunc libero nibh, aliquet sit amet tellus non, tincidunt facilisis lectus.
                                    Ut suscipit lorem est, eget varius nulla tempor sed. Etiam eget blandit erat. Nam interdum dui sed nibh feugiat malesuada.
                                    Aliquam nulla diam, auctor sit amet finibus vel, facilisis vitae tortor. Morbi at augue sit amet lectus tincidunt mollis.
                                    Proin eu aliquet nunc.
                                </p>
                                <Link className="btn btn-primary" to={{ pathname: Routes.PrivatisationPage }}>Privatiser</Link>
                            </div>
                        </div>
                    </div>
                    {/* <Link to={Routes.ExemplePage} message="test">Go to exemple page</Link> */}
                </div>
                <FooterComponent></FooterComponent>

            </div>

        );
    }
}

export default HomePage;