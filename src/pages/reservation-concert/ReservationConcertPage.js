import './ReservationConcertPage.scss';
import { NavbarComponent } from '../../components/navbar/NavbarComponent'
import { BasePage } from '../_base-page';
import { FooterComponent } from '../../components/footer/FooterComponent';
import { ConcertService } from '../../services/concert.service';
import { LoaderComponent } from '../../components/loader/LoaderComponent';
import { Link, Redirect } from 'react-router-dom';
import { Routes } from '../../constantes/routes';
import Moment from 'react-moment';
import { RefValueService } from '../../services/refValue.service';
import mapboxgl from 'mapbox-gl';
import { BreadCrumbComponent } from '../../components/breadcrump/BreadCrumbComponent';
import { OverlayTrigger, Tooltip } from 'react-bootstrap';
import { ConcertTicketReservationService } from '../../services/concertTicketReservation.service';

class ReservationConcertPage extends BasePage {

    concertId;
    state;
    planRow = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I'];
    planColumnNb = 12;
    planTickets;

    constructor() {
        super();
        this.state = {
            concert: undefined,
            loaded: false,
            planTickets: [],
            checkedTickets: [],
            totalCheckedPrice: 0,
            obtainingMethod: [],
            selectedObtainingMethod: undefined,
            categories: [],
            lng: 5.4474,
            lat: 43.5297,
            zoom: 12,
            step: 1,
            calculatedPrices: [],
            decreasingPercentage: undefined,
            maxPrice: undefined,
            redirect: null,
            ticketsReservation: [],
        };
    }

    async componentDidMount() {
        // Mapbox
        const map = new mapboxgl.Map({
            container: this.mapContainer,
            style: 'mapbox://styles/mapbox/streets-v11',
            center: [this.state.lng, this.state.lat],
            zoom: this.state.zoom,
        });

        const concertResponse = await ConcertService.getConcert(this.concertId);
        console.log("\n \n ~ concertResponse", concertResponse)
        if (concertResponse?.data) {
            this.setState({
                concert: concertResponse.data,
                maxPrice: concertResponse.data.maxPrice,
                decreasingPercentage: concertResponse.data.decreasingPercentage,
            });
        }

        const categoriesResponse = await RefValueService.getAllOfTypeCode('ConcertTicketCategoryCode');
        console.log("\n \n ~ categoriesResponse", categoriesResponse)
        if (categoriesResponse?.data) {
            this.setState({ categories: categoriesResponse.data });
        }

        const obtainingMethodsReponse = await RefValueService.getAllOfTypeCode('ObtainingMethodCategoryCode');
        console.log("\n \n ~ obtainingMethodsReponse", obtainingMethodsReponse)
        if (obtainingMethodsReponse?.data) {
            const sortedData = [];
            sortedData.push(obtainingMethodsReponse.data.find(x => x.code === "ObtainingMethodCategoryETicket"));
            sortedData.push(obtainingMethodsReponse.data.find(x => x.code === "ObtainingMethodCategoryGuichet"));
            sortedData.push(obtainingMethodsReponse.data.find(x => x.code === "ObtainingMethodCategoryEnvoi"));
            this.setState({ obtainingMethod: sortedData });
        }

        // const ticketsReservationResponse = await ConcertTicketReservationService.getAllTicketReservations();
        // console.log("\n \n ~ ticketsReservationResponse", ticketsReservationResponse)
        // if (ticketsReservationResponse?.data) {
        //     this.setState({ ticketsReservation: ticketsReservationResponse.data });
        // }

        this.buildTickets();
        this.calculatePrices();
        this.setState({ loaded: true });
    }

    getTicketPrice(row) {
        let cpt;
        switch (row) {
            case 'A': cpt = 0; break;
            case 'B': cpt = 1; break;
            case 'C': cpt = 2; break;
            case 'D': cpt = 3; break;
            case 'E': cpt = 4; break;
            case 'F': cpt = 5; break;
            case 'G': cpt = 6; break;
            case 'H': cpt = 7; break;
            case 'I': cpt = 8; break;
            default: break;
        }

        if (cpt) {
            return (this.state.maxPrice * (100 - (this.state.decreasingPercentage * cpt)) / 100);
        }

        return this.state.maxPrice;
    }

    buildTickets() {
        this.planTickets = [];
        for (const item of this.planRow) {
            const array = [];
            for (let index = 1; index <= this.planColumnNb; index++) {
                const elem = { row: item, span: index, booked: false, checked: false, key: item + '_' + index };
                if (item === 'A' || item === 'B' || item === 'C') {
                    elem.category = this.state.categories.find(x => x.code === "ConcertTicketCategoryCat1");
                }

                if (item === 'D' || item === 'E' || item === 'F') {
                    elem.category = this.state.categories.find(x => x.code === "ConcertTicketCategoryCat2");
                }

                if (item === 'G' || item === 'H' || item === 'I') {
                    elem.category = this.state.categories.find(x => x.code === "ConcertTicketCategoryCat3");
                }

                // elem.price = this.state.concert.concertTickets.find(x => x.category.id === elem.category.id).price;
                elem.price = this.getTicketPrice(item);

                array.push(elem);
            }
            this.planTickets.push(array);
        }

        // todo : call back pour mettre booked true si deja des reservations
        // checked true si booked

        this.setState({ planTickets: this.planTickets });
    }

    getCheckedTickets() {
        let checked = [];
        for (const item of this.state.planTickets) {
            const filter = item.filter(x => x.checked);
            checked.push(...filter);
        }

        console.log("\n \n ~ checked", checked);

        let total = 0;
        for (const item of checked) {
            total += item.price;
        }

        this.setState({ checkedTickets: checked, totalCheckedPrice: total });
    }

    handleChangeChk(key) {
        let findElem;
        for (const item of this.state.planTickets) {
            if (!findElem)
                findElem = item.find(x => x.key === key);
        }

        findElem.checked = !findElem.checked;
        this.setState({ planTickets: this.state.planTickets });
        this.getCheckedTickets();
    }

    calculatePrices() {
        if (!this.state.maxPrice || !this.state.decreasingPercentage) {
            this.setState({ calculatedPrices: [] });
            return;
        }

        const prices = [];
        const rowNbPerCategory = 3;
        let currentMaxPrice = parseInt(this.state.maxPrice.toString(), 10);
        let currentMinPrice = this.state.maxPrice;
        let cpt = 1;
        let categoriesCpt = 1;
        for (const item of this.state.categories) {
            for (let j = 0; j < rowNbPerCategory; j++) {
                if (categoriesCpt === 1 && j === 0) {
                    currentMinPrice = this.state.maxPrice;
                } else {
                    currentMinPrice = (this.state.maxPrice * (100 - (this.state.decreasingPercentage * cpt)) / 100);
                    cpt++;
                }
            }

            prices.push({ category: item, min: currentMinPrice, max: currentMaxPrice });
            currentMaxPrice = (this.state.maxPrice * (100 - (this.state.decreasingPercentage * cpt)) / 100);
            categoriesCpt++;

            // console.log("\n \n ~ prices", prices);
        }

        this.setState({ calculatedPrices: prices });
    }

    validate(state) {
        let errorMsg = '';
        if (!state.selectedObtainingMethod) {
            errorMsg += 'Veuillez sélectionner un moyen d\'obtention\n';
        }

        if (!state.checkedTickets?.length) {
            errorMsg += 'Veuillez sélectionner au moins 1 ticket\n';
        }

        if (errorMsg) {
            alert(errorMsg);
        }

        const elems = [];
        for (const item of state.checkedTickets) {
            const elem = {};
            elem.obtainingMethod = state.selectedObtainingMethod;
            elem.spanNumber = item.span;
            elem.rowNumber = item.row;
            elem.concertTicket = state.concert.concertTickets.find(x => x.category.id === item.category.id);
            elem.price = item.price;

            elems.push(elem);
        }

        localStorage.setItem('ticketsReservation', JSON.stringify(elems));
        const route = Routes.Cart + "/" + this.concertId;
        this.setState({ redirect: route });
    }

    selectObtainingMethod(value) {
        this.setState({ selectedObtainingMethod: value });
    }

    render() {
        if (this.state.redirect) {
            console.log(this.state.redirect);
            return <Redirect to={this.state.redirect} />
        }
        const { match: { params } } = this.props;
        this.concertId = params.id;

        return (
            <div>
                {!this.state.loaded ? <LoaderComponent></LoaderComponent> : null}

                <NavbarComponent />

                <div className="ReservationConcert">
                    <BreadCrumbComponent selectedStep={this.state.step}></BreadCrumbComponent>

                    <div className="">
                        <div className="my-5 concert-info">
                            <div className="container">
                                <div className="row concert-info__header">
                                    <div className="col-12 col-md-4">
                                        <p>{this.state.concert?.musicGroup?.name}</p>
                                        <div className="artist-picture" style={{ backgroundImage: 'url(' + this.state.concert?.image + ')' }}></div>
                                    </div>
                                    <div className="col-12 col-md-4 concert-info__header-txt">
                                        <p>
                                            <Moment format="D MMMM YYYY à HH:mm" locale="fr">{this.state.concert?.date}</Moment>
                                        </p>
                                        <p>{this.state.concert?.concertHall?.location?.name} ({this.state.concert?.concertHall?.name})</p>
                                        <p>{this.state.concert?.musicCategory?.value}</p>
                                    </div>
                                    <div ref={el => this.mapContainer = el} className="mapContainer col-12 col-md-4" />
                                </div>
                            </div>
                        </div>

                        <div className="plan container">
                            <h2>1. Choisissez vos places sur le plan :</h2>

                            <div className="row mt-5">
                                <div className="plan__seat-prices col-md-2 col-sm-12">
                                    {this.state.calculatedPrices ? this.state.calculatedPrices.map(x => (
                                        <div>
                                            {x.category?.value} - {x.min} € à {x.max} €
                                        </div>
                                    )) : ''}
                                </div>

                                {/* <TicketsPlanComponent planTickets={this.state.planTickets}></TicketsPlanComponent> */}

                                <div className="plan__seatPicker-container col-md-10 col-sm-12">
                                    <div className="plan__seatPicker-header">scène</div>

                                    <div className="plan__seatPicker">
                                        <div className="left-col">
                                            <span>A</span>
                                            <span>B</span>
                                            <span>C</span>
                                            <span>D</span>
                                            <span>E</span>
                                            <span>F</span>
                                            <span>G</span>
                                            <span>H</span>
                                            <span>I</span>
                                        </div>

                                        <div className="top-col">
                                            <span>1</span>
                                            <span>2</span>
                                            <span>3</span>
                                            <span>4</span>
                                            <span>5</span>
                                            <span>6</span>
                                            <span>7</span>
                                            <span>8</span>
                                            <span>9</span>
                                            <span>10</span>
                                            <span>11</span>
                                            <span>12</span>
                                        </div>

                                        <div className="plan__seatPicker-places">
                                            {this.state.planTickets ? this.state.planTickets.map(x => (
                                                <div className="flex">
                                                    {x.map(y => (
                                                        <ul>
                                                            <OverlayTrigger
                                                                placement="top"
                                                                overlay={
                                                                    <Tooltip id="tooltip-top">
                                                                        {y.price}€
                                                                    </Tooltip>
                                                                }
                                                            >
                                                                <li key={y.key} >
                                                                    <input aria-label={(y.booked ? 'disabled' : '')} type="checkbox" key={'input_' + y.key} checked={y.checked} onChange={() => this.handleChangeChk(y.key)} disabled={y.booked} />
                                                                    <label aria-label={(y.booked ? 'disabled' : '')} htmlFor="checkbox-p" key={'label_' + y.key}></label>
                                                                </li>
                                                            </OverlayTrigger>
                                                        </ul>
                                                    ))}
                                                </div>
                                            )) : ''}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="recap-plan my-5 border">
                                <p>Nombre de places choisies :</p>

                                <table>
                                    <tbody>
                                        {this.state.checkedTickets ? this.state.checkedTickets.map(x => (
                                            <tr>
                                                <td className="center">1.</td>
                                                <td className="center middle">Place {x.span}, Rang {x.row}</td>
                                                <td>{x.price}€</td>
                                            </tr>
                                        )) : ''}
                                        <tr>
                                            <td></td>
                                            <td className="right middle">Total</td>
                                            <td>{this.state.totalCheckedPrice}€</td>
                                        </tr>
                                    </tbody>
                                </table>

                                <p className="m-0 mt-4">Vous avez choisi {this.state.checkedTickets.length} places pour un montant total de {this.state.totalCheckedPrice}€</p>
                            </div>
                        </div>

                        <div className="ticket container">
                            <h2>2. Choisissez le mode d'obtention des billets :</h2>

                            <div className="reservation-tickets">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td> <input className="form-check-input" type="radio" name="obtention" onChange={() => this.selectObtainingMethod(this.state.obtainingMethod[0])}></input>E-Ticket (gratuit)</td>
                                            <td>Imprimez vos billets chez vous dès la fin de votre commande et recevez-les également par e-mail en format pdf.</td>
                                        </tr>
                                        <tr>
                                            <td> <input className="form-check-input" type="radio" name="obtention" onChange={() => this.selectObtainingMethod(this.state.obtainingMethod[1])}></input>Retrait au guichet (1,80€)</td>
                                            <td>Retirez vos billets auprès de nos guichets (comprend des frais de transaction).</td>
                                        </tr>
                                        <tr>
                                            <td> <input className="form-check-input" type="radio" name="obtention" onChange={() => this.selectObtainingMethod(this.state.obtainingMethod[2])}></input>Envoi postal (8,00€)</td>
                                            <td>Recevez vos billets à votre domicile ou sur votre lieu de travail. Envoi suivi avec remise contre signature. Livraison sous 24 à 48h.</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div className="button-container container">
                            <Link className="btn btn-danger" to={{ pathname: Routes.DetailConcert + '/' + this.concertId }}>ANNULER</Link>
                            <button className="btn btn-primary" onClick={() => this.validate(this.state)}>VALIDER</button>
                        </div>
                    </div >
                </div >

                <FooterComponent></FooterComponent>
            </div >
        );
    }
}

export default ReservationConcertPage;