import './ExemplePage.scss';
import { ExempleService } from '../../services/exemple.service';
import { Link } from "react-router-dom";
import { BasePage } from '../_base-page';
import { Routes } from '../../constantes/routes';
import { CardComponent } from '../../components/card/CardComponent';
import { ConcertService } from '../../services/concert.service';
import { LocationService } from '../../services/location.service';

class ExemplePage extends BasePage {
    constructor() {
        super();
    }

    render() {
        return (
            <div className="ExemplePage">
                <h1>Exemple Page</h1>
                <CardComponent text="hello world" />

                <Link to={Routes.Home}>Go back</Link>
                <button onClick={LocationService.getAllLocations}>Exemple service : Log</button>
                <Link className="btn btn-primary" to={{ pathname: Routes.Programmation, data: { location: 'all' } }}>Bourges</Link>

            </div>
        );
    }
}

export default ExemplePage;