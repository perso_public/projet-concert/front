import './DetailConcertPage.scss';
import { NavbarComponent } from '../../components/navbar/NavbarComponent'
import { BasePage } from '../_base-page';
import { FooterComponent } from '../../components/footer/FooterComponent';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/swiper.scss';
import { VignetteConcertComponent } from '../../components/vignettes/VignetteConcertComponent';
import { ConcertService } from '../../services/concert.service';
import { Routes } from '../../constantes/routes';
import { Link } from 'react-router-dom';
import { LoaderComponent } from '../../components/loader/LoaderComponent';
import Moment from 'react-moment';

class DetailConcertPage extends BasePage {
    concertId;
    state;

    constructor() {
        super();
        this.state = {
            concert: undefined,
            concerts: [],
            loaded: false,
        };
    }

    async componentDidMount() {
        const concertResponse = await ConcertService.getConcert(this.concertId);
        console.log("\n \n ~ concertResponse", concertResponse)
        if (concertResponse?.data) {
            this.setState({ concert: concertResponse.data });
        }

        const concertsResponse = await ConcertService.getAllConcerts();
        console.log("\n \n ~ concertResponse", concertsResponse)
        if (concertsResponse?.data) {
            this.setState({ concerts: concertsResponse.data });
        }

        this.setState({ loaded: true });
    }

    render() {
        const { match: { params } } = this.props;
        this.concertId = params.id;

        return (
            <div>
                {!this.state.loaded ? <LoaderComponent></LoaderComponent> : null}

                <NavbarComponent />

                <div className="DetailConcert">
                    <div className="header row my-5 container-fluid">
                        <div className="picture col-12 col-md-4 offset-0 offset-md-2" style={{ backgroundImage: 'url(' + this.state.concert?.image + ')' }}></div>
                        <div className="concert-info col-12 col-md-6">
                            <p>{this.state.concert?.musicGroup?.name}</p>
                            <p>{this.state.concert?.name}</p>
                            <p>
                                <Moment format="D MMMM YYYY à HH:mm" locale="fr">{this.state.concert?.date}</Moment>
                            </p>
                            <p>{this.state.concert?.concertHall?.name} ({this.state.concert?.concertHall?.location?.name})</p>
                            <p>{this.state.concert?.musicCategory?.value}</p>
                        </div>
                    </div>

                    <div className="my-5 py-5 container">
                        <table className="table concert-table">
                            <thead>
                                <tr>
                                    <th scope="col">Date</th>
                                    <th scope="col">Lieu</th>
                                    <th scope="col">Heure</th>
                                    <th scope="col">Ouverture</th>
                                    <th scope="col">Catégorie de tarifs</th>
                                    <th scope="col">Tarifs</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <Moment format="D MMMM YYYY à HH:mm" locale="fr">{this.state.concert?.date}</Moment>
                                    </td>
                                    <td>{this.state.concert?.concertHall?.location?.name}</td>
                                    <td>
                                        <Moment format="HH:mm" locale="fr">{this.state.concert?.date}</Moment>
                                    </td>
                                    <td>
                                        <Moment format="HH:mm" locale="fr">{this.state.concert?.openingHours}</Moment>
                                    </td>
                                    <td>{ConcertService.getCategoriesString(this.state.concert)}</td>
                                    <td>De {this.state.concert?.minPrice}€ à  {this.state.concert?.maxPrice}€</td>
                                    <td>
                                        <Link className="btn btn-primary" to={{ pathname: Routes.ReservationConcert + '/' + this.concertId }}>Réserver</Link>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div className="artist-info pb-5 container">
                        <div className="col-12">
                            <h3>Présentation de l'artiste / groupe</h3>
                            <p className="mt-4" dangerouslySetInnerHTML={{ __html: this.state.concert?.musicGroup?.description }}>
                                {/* {this.state.concert?.musicGroup?.description} */}
                            </p>
                        </div>
                        {/* <div className="col-12 col-md-6">
                            <iframe width="100%" height="100%" src="https://www.youtube.com/embed/-CVn3-3g_BI" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
                        </div> */}
                    </div>

                    {/* <div className="reservation row my-5 py-5">
                        <div className="parking col-12 col-md-4 offset-0 offset-md-2 px-4">
                            <h4>Réserver une place de Parking</h4>
                            <p className="my-3">Réservez votre place de parking dès à présent et facilitez-vous l'accès à nos salles de concert</p>
                            <button className="btn btn-primary">Réserver</button>
                        </div>
                        <div className="restaurant col-12 col-md-4 px-4">
                            <h4>Réserver au Restaurant</h4>
                            <p className="my-3">Réservez votre place dans nos restaurants pour profiter de nos mets préparez par nos grands chefs.</p>
                            <button className="btn btn-primary">Réserver</button>
                        </div>
                    </div> */}

                    <div className="not-to-miss my-5 py-5 container">
                        <h3>À ne pas manquer</h3>
                        {this.state.concerts?.length ?
                            <Swiper spaceBetween={50} slidesPerView={3}>
                                {this.state.concerts ? this.state.concerts.map(x => (
                                    <SwiperSlide><VignetteConcertComponent concert={x}></VignetteConcertComponent></SwiperSlide>
                                )) : ''}
                            </Swiper>
                            : ''}
                    </div>
                </div>

                <FooterComponent></FooterComponent>
            </div>
        );
    }
}

export default DetailConcertPage;