import './DetailConcert.scss';
import { BasePage } from '../_base-page';

class DetailConcertPage extends BasePage {
    constructor() {
        super();
    }

    render() {
        return (
            <div className="DetailConcertPage">
                <h1>Detail Concert</h1>
            </div>
        );
    }
}

export default DetailConcertPage;