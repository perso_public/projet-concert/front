import axios from 'axios';
import { Api } from '../constantes/api';

export class ConcertReservationService {
    static async postConcertReservation(data) {
        const url = Api.Url + '/concert_reservations';

        return await axios.post( url, data);
    }
}