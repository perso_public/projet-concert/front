import axios from 'axios';
import { Api } from '../constantes/api';

export class ConcertTicketReservationService {
    static async postConcertTicketReservation(data) {
        const url = Api.Url + '/concert_ticket_reservations';

        return await axios.post(url, data);
    }

    static async getAllTicketReservations() {
        const url = Api.Url + '/concert_ticket_reservations';

        return await axios.get(url);
    }
}