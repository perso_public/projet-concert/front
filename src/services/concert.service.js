import axios from 'axios';
import { Api } from '../constantes/api';

export class ConcertService {
    static async getAllConcerts(filter, order) {
        let url = Api.Url + '/concerts?';
        if (filter) {
            if (filter['musicCategory'] && filter['musicCategory'] !== 'all') {
                url += '&musicCategory.value=' + filter['musicCategory'];
            }
            if (filter['location'] && filter['location'] !== 'all') {
                url += '&concertHall.location.name=' + filter['location'];
            }
            if (filter['dateBefore']) {
                url += '&date[before]=' + filter['dateBefore'];
            }
            if (filter['dateAfter']) {
                url += '&date[after]=' + filter['dateAfter'];
            }
        }

        if (order) {
            if (order['date']) {
                url += '&order[date]=' + order['date'];
            }
            else if (order['musicGroup']) {
                url += '&order[musicGroup.name]=' + order['musicGroup'];
            }
        }
        console.log('url : ' + url);
        return await axios.get(url);
    }

    static async getConcert(id) {
        return await axios.get(Api.Url + '/concerts/' + id);
    }

    static getCategoriesMinPrice(concert) {
        if (!concert)
            return;

        let minPrice = -1;
        for (const item of concert.concertTickets) {
            if (minPrice === -1 || item.price < minPrice)
                minPrice = item.price;
        }

        return minPrice;
    }

    static getCategoriesMaxPrice(concert) {
        if (!concert)
            return;

        let maxPrice = -1;
        for (const item of concert.concertTickets) {
            if (maxPrice === -1 || item.price > maxPrice)
                maxPrice = item.price;
        }

        return maxPrice;
    }

    static getCategoriesString(concert) {
        if (!concert)
            return;

        let str = '';
        for (const item of concert.concertTickets) {
            str += item.category.value + ', ';
        }

        return str;
    }

    static async createConcert(concert) {
        let url = Api.Url + '/concerts';

        return await axios.post(url, concert, {
            headers: {
                'Accept': 'application/json',
            },
        });
    }

    static async modifyConcert(id, concert) {
        let url = Api.Url + '/concerts/' + id;

        return await axios.put(url, concert, {
            headers: {
                'Accept': 'application/json',
            },
        });
    }
}