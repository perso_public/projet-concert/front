import axios from 'axios';
import { Api } from '../constantes/api';

export class RefValueService {
    static async getAllOfTypeCode(typeCode) {
        return await axios.get(Api.Url + '/ref_values?typeCode=' + typeCode);
    }
}