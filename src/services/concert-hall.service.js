import axios from 'axios';
import { Api } from '../constantes/api';

export class ConcertHallService {
    static async getAllConcertHalls() {
        return await axios.get(Api.Url + '/concert_halls');
    }

    static async getConcertHall(id) {
        return await axios.get(Api.Url + '/concert_halls/' + id);
    }
}