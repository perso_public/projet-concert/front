import axios from 'axios';
import { Api } from '../constantes/api';

export class LocationService {
    static async getAllLocations() {
        return await axios.get(Api.Url + '/locations');
    }
}