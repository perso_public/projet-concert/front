import axios from 'axios';
import { Api } from '../constantes/api';

export class ReservationCoordonneesService {
    static async updateUserData(data) {
        const url = Api.Url + '/user/update';

        return await axios.put( url, data, {headers: {
                'Accept':'application/json'
            }
        });
    }
}